<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Model.DoanhNghiepModel" %>
<%@page import="Access.DoanhNghiepAccess" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Chi tiết | Quản lý doanh nghiệp</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../../plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../../plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../../plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../../plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- layout header -->
            <jsp:include page="../Layout/header.jsp"></jsp:include>
                <!-- end layout header -->

                <!-- menu bar -->
            <jsp:include page="../Layout/menubar.jsp"></jsp:include>
                <!-- end menu bar -->
            <%
                DoanhNghiepAccess dn = new DoanhNghiepAccess();
                DoanhNghiepModel dnmd = new DoanhNghiepModel();
                String MaDN = "";
                if (request.getParameter("maDN") != null){
                    MaDN = request.getParameter("maDN");
                    dnmd = dn.getDNbyidDN(Integer.parseInt(MaDN));
                }
            %>
            <!-- body -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content-header">
                    <h1>
                        Chi tiết thông tin doanh nghiệp
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Quản Lý Doanh Nghiệp</a></li>
                        <li class="active">Chi tiết</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-md-3">
                            <form action="DoanhNghiepServlet" method="get">
                            <!-- Profile Image -->
                            <div class="box box-primary">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="../../images/blog/16729507_1862293323986653_1613460087863061227_n.jpg" alt="User profile picture">

                                    <h3 class="profile-username text-center"><%=dnmd.getTenDN()%></h3>

                                    <p class="text-muted text-center">Kinh Doanh</p>

                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b>Mã Doanh Nghiệp</b> <a class="pull-right"><%=dnmd.getMaDN()%></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Mã Thuế</b> <a class="pull-right"><%=dnmd.getMaThue()%></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Vốn</b> <a class="pull-right"><%=dnmd.getVon()%></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Mã bài đăng</b> <a class="pull-right"><%=dnmd.getMaBD()%></a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Khu Vực</b> <a class="pull-right">Tên Khu Vực</a>
                                        </li>
                                    </ul>

                                    <a href="chinhsuadoanhnghiep.jsp?maDN=<%=dnmd.getMaDN()%>" class="btn btn-primary btn-block"><b>Chỉnh sửa thông tin</b></a>
                                    <a href="/business/DoanhNghiepServlet?command=delete&MaDN=<%=dnmd.getMaDN()%>" class="btn btn-danger btn-block" style="color: fff"><b>Xóa doanh nghiệp</b></a>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </form>
                            <!-- /.box -->

                            <!-- About Me Box -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Thông tin khác</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <strong><i class="fa fa-book margin-r-5"></i> Điện Thoại</strong>

                                    <p class="text-muted">
                                        <%=dnmd.getDienThoai()%>
                                    </p>

                                    <hr>

                                    <strong><i class="fa fa-map-marker margin-r-5"></i> Địa Chỉ</strong>

                                    <p class="text-muted"><%=dnmd.getDiaChi()%></p>

                                    <hr>

<!--                                    <strong><i class="fa fa-pencil margin-r-5"></i> Lĩnh Vực</strong>

                                    <p>
                                        <span class="label label-danger">UI Design</span>
                                        <span class="label label-success">Coding</span>
                                        <span class="label label-info">Javascript</span>
                                        <span class="label label-warning">PHP</span>
                                        <span class="label label-primary">Node.js</span>
                                    </p>-->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#activity" data-toggle="tab">Bài đăng</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="active tab-pane" id="activity">
                                        <!-- Post -->
                                        <div class="post">
                                            <div class="user-block">
                                                <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                                                <span class="username">
                                                    <a href="#">Nội Dung Bài Viết</a>
                                                </span>
                                                <span class="description">Được đăng lúc - 7:30 PM today</span>
                                            </div>
                                            <!-- /.user-block -->
                                            <p>
                                                Lorem ipsum represents a long-held tradition for designers,
                                                typographers and the like. Some people hate it and argue for
                                                its demise, but others ignore the hate as they create awesome
                                                tools to help create filler text for everyone from bacon lovers
                                                to Charlie Sheen fans.
                                            </p>
                                        </div>
                                        <!-- /.post -->

                                        <!-- Post -->
                                        <div class="post">
                                            <div class="user-block">
                                                <span class="username">
                                                    <a>Hình Ảnh</a>
                                                </span>
                                            </div>
                                            <!-- /.user-block -->
                                            <div class="row margin-bottom">
                                                <div class="col-sm-6">
                                                    <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
                                                            <br>
                                                            <img class="img-responsive" src="../../dist/img/photo3.jpg" alt="Photo">
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-sm-6">
                                                            <img class="img-responsive" src="../../dist/img/photo4.jpg" alt="Photo">
                                                            <br>
                                                            <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                        </div>
                                        <!-- /.post -->
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.nav-tabs-custom -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                </section>
                <!-- end Main content -->
            </div>
            <!-- end body -->

            <!-- layout footer -->
            <jsp:include page="../Layout/footer.jsp"></jsp:include>
                <!-- end layout footer -->

                <!-- Control Sidebar -->
            <jsp:include page="../Layout/control-bar.jsp"></jsp:include>
            <!-- /.control-sidebar -->


            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 2.2.3 -->
        <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="../../plugins/morris/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="../../plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="../../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="../../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="../../plugins/knob/jquery.knob.js"></script>
        <!-- daterangepicker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="../../plugins/daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../../plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="../../dist/js/app.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="../../dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
    </body>
</html>
