<%-- 
    Document   : menubar
    Created on : Apr 21, 2017, 4:30:12 PM
    Author     : VIETANH
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Việt Anh</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">QUẢN LÝ BUSINESS</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Thống kê</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="../Quanlythongke/thongke.jsp"><i class="fa fa-circle-o"></i> Chi tiết</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý tài khoản</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlytaikhoan/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlytaikhoan/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý khu vực</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlykhuvuc/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlykhuvuc/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Quản lý lĩnh vực</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlylinhvuc/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlylinhvuc/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Quản lý vốn</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlyvon/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlyvon/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Quản lý bài đăng</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlybaidang/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Quản lý doanh nghiệp</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlydoanhnghiep/danhsachdoanhnghiep.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Quản lý tin tức</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlytintuc/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlytintuc/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Quản lý quảng cáo</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlyquangcao/danhsach.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
            <li><a href="../Quanlyquangcao/them.jsp"><i class="fa fa-circle-o"></i> Thêm</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Thông tin website</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../Quanlythontinweb/chitiet.jsp"><i class="fa fa-circle-o"></i> Danh sách</a></li>
          </ul>
        </li>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>