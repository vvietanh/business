﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Hoxa - Multipurpose HTML5 Template</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../../images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link href="../../css/reset.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />
    
    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->
    
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="../../s/slidepanel/slidepanel.css">
    
	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/tabwidget/tabwidget.css" />

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../s/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
	
 
</head>

<body>


<div class="site_wrapper">


<!-- header -->
<jsp:include page="../Layout/header.jsp"></jsp:include>
<!-- end header -->

<div class="clearfix"></div>

<div class="page_title2">
<div class="container">

    <div class="two_third">
    
    	<div class="title"><h1>Hai công ty mía đường của ông Đặng Văn Thành sẽ sáp nhập thành 1 công ty có quy mô gần nửa tỷ đô</h1></div>
        
        <div class="pagenation">&nbsp;<a href="index.html">Trang chủ</a> <i>/</i> <a href="#">Tin tức</a> <i>/</i> Hai công ty mía đường của ông Đặng Văn Thành sẽ sáp nhập thành 1 công ty có quy mô gần nửa tỷ đô</div>
        
    </div>
    

</div>
</div><!-- end page title --> 

<div class="clearfix"></div>

<div class="container">

	<div class="content_left">
        	
        <div class="blog_post">	
            <div class="blog_postcontent">
            <div class="image_frame"><a href="#"><img src="../../images/tintuc1.jpg" alt="" /></a></div>
    
                <ul class="post_meta_links">
                    <li><a href="#" class="date">19-04-2017</a></li>
                    <li class="post_by"><i>by:</i> <a href="#">Adam Harrison</a></li>
                    <li class="post_categoty"><i>in:</i> <a href="#">Web tutorials</a></li>
                    <li class="post_comments"><i>note:</i> <a href="#">18 Comments</a></li>
                </ul>
             <div class="clearfix"></div>
             <div class="margin_top1"></div>
            <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years <a href="#">xem thêm...</a></p>
            </div>
        </div><!-- /# end post -->
            
            <div class="clearfix divider_dashed9"></div>

            
            <div class="sharepost">
				<h4>Share bài viết</h4>
					<ul>
						<li><a href="#">&nbsp;<i class="fa fa-facebook fa-lg"></i>&nbsp;</a></li>
						<li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-flickr fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-html5 fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-skype fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube fa-lg"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-linux fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-android fa-lg"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss fa-lg"></i></a></li>
					</ul>
				
				</div><!-- end share post links -->
                
                <div class="clearfix"></div>
                
            <h4>Thông tin tác giả</h4>
            <div class="about_author">
            
                <img src="../../images/vietanh.jpg" alt="" />
                
                <a href="http://themeforest.net/user/gsrthemes9/portfolio" target="_blank">Việt Anh</a><br />
               I'm a freelance designer with satisfied clients worldwide. I design simple, clean websites and develop easy-to-use applications. Web Design is not just my job it's my passion. You need professional web designer you are welcome.
            </div><!-- end about author -->
            
            <div class="clearfix margin_top5"></div>
            
    <div class="clearfix divider_dashed9"></div>
       
   <h4>Bình luận</h4>
	<div class="mar_top_bottom_lines_small3"></div>
	<div class="comment_wrap">
		<div class="gravatar"><img src="../../images/blog/people_img.jpg" alt="" /></div>
			<div class="comment_content">
				<div class="comment_meta">

					<div class="comment_author">Vi Ngô - <i>20/04/2017</i></div>                   
                    
				</div>
				<div class="comment_text">
					<p>Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage.</p>
					<a href="#">Trả lời</a>
				</div>
			</div>
		</div><!-- end section -->
		
	<div class="comment_wrap chaild">
		<div class="gravatar"><img src="../../images/vietanh.jpg" alt="" /></div>
			<div class="comment_content">
				<div class="comment_meta">

					<div class="comment_author">Việt Anh - <i>20/04/2017</i></div>                   
                    
				</div>
				<div class="comment_text">
					<p>Lorem ipsum dolor sit amet, consectetur rius a auctor enim accumsan.</p>
					<a href="#">Trả lời</a>
				</div>
			</div>
		</div><!-- end section -->
        
        <div class="comment_wrap chaild">
		<div class="gravatar"><img src="../../images/vietanh.jpg" alt="" /></div>
			<div class="comment_content">
				<div class="comment_meta">

					<div class="comment_author">Việt Anh - <i>20/04/2017</i></div>                   
                    
				</div>
				<div class="comment_text">
					<p>Lorem ipsum dolor sit amet, consectetur rius a auctor enim accumsan.</p>
					<a href="#">Trả lời</a>
				</div>
			</div>
		</div><!-- end section -->
		
	
		<div class="comment_form">
			
			<h4>Để lại bình luận</h4>
			
			<form action="blog-post.html" method="post">
					<input type="text" name="yourname" id="name" class="comment_input_bg" />
					<label for="name">Tên*</label>
					
					<input type="text" name="email" id="mail" class="comment_input_bg" />
					<label for="mail">Email*</label>
					
					<input type="text" name="website" id="website" class="comment_input_bg" />
					<label for="website">Website</label>
					
					<textarea name="comment" class="comment_textarea_bg" rows="20" cols="7" ></textarea>
					<div class="clearfix"></div> 
					<input name="send" type="submit" value="Bình luận" class="comment_submit"/>
					<p></p>
				
				
			</form>
			
			</div><!-- end comment form -->
            
	<div class="clearfix mar_top2"></div>  
            
</div><!-- end content left side -->

<!-- right sidebar starts -->
<div class="right_sidebar">
	
    <div class="sidebar_widget">
    
    	<div class="sidebar_title"><h4>Site <i>Categories</i></h4></div>
		<ul class="arrows_list1">		
            <li><a href="#"><i class="fa fa-caret-right"></i> Economics</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Social Media</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Economics</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Online Gaming</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Entertainment</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Technology</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Make Money Online</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Photography</a></li>
            <li><a href="#"><i class="fa fa-caret-right"></i> Web Tutorials</a></li>
		</ul>
        
	</div><!-- end section -->
    
    <div class="clearfix margin_top4"></div>
    
    <div class="sidebar_widget">
    
    	<div id="tabs">
        
			<ul class="tabs">  
				<li class="active"><a href="#tab1">Phổ biến</a></li>
				<li><a href="#tab2">Gần đây</a></li>
				<li class="last"><a href="#tab3">Tags</a></li>
			</ul><!-- /# end tab links --> 
 
		<div class="tab_container">	
			<div id="tab1" class="tab_content"> 
            
				<ul class="recent_posts_list">
					<li>
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Publishing packag esanse web page editos</a>
						 <i>July 13, 2014</i> 
					</li>	
					<li>
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Sublishing packag esanse web page editos</a>
						 <i>July 12, 2014</i> 
					</li>	
					<li class="last">
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Mublishing packag esanse web page editos</a>
						 <i>July 11, 2014</i> 
					</li>
				</ul>
                 
			</div><!-- end popular posts --> 
			
			<div id="tab2" class="tab_content">	 
				<ul class="recent_posts_list">
                
					<li>
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Various versions has evolved over the years</a>
						 <i>July 18, 2014</i> 
					</li>
					<li>
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Rarious versions has evolve over the years</a>
						 <i>July 17, 2014</i> 
					</li>
					<li class="last">
					  	<span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
						<a href="#">Marious versions has evolven over the years</a>
						 <i>July 16, 2014</i> 
					</li>
				</ul>
                 
			</div><!-- end popular articles -->	
			
			<div id="tab3" class="tab_content">	 
				<ul class="tags">
														
					<li><a href="#">2014</a></li>
					<li><a href="#"><b>Amazing</b></a></li>
					<li><a href="#">Animation</a></li>
					<li><a href="#">Beautiful</a></li>
					<li><a href="#"><b>Cartoon</b></a></li>
					<li><a href="#">Comedy</a></li>
					<li><a href="#"><b>Cool</b></a></li>
					<li><a href="#">Dance</a></li>
					<li><a href="#">Drive</a></li>
					<li><a href="#"><b>Family</b></a></li>
					<li><a href="#"><b>Fantasy</b></a></li>
					<li><a href="#">News</a></li>
					<li><a href="#"><b>Friends</b></a></li>
					<li><a href="#">Funny</a></li>
					<li><a href="#"><b>Games</b></a></li>
					<li><a href="#">Love</a></li>
					<li><a href="#"><b>Music</b></a></li>
					<li><a href="#">Nature</a></li>
					<li><a href="#"><b>Party</b></a></li>
					<li><a href="#">Pictures</a></li>
					<li><a href="#">Sports</a></li>
					<li><a href="#"><b>Best</b></a></li>
					<li><a href="#">Wedding</a></li>
					<li><a href="#">Weight</a></li>
					<li><a href="#"><b>Youtube</b></a></li>
				</ul>	 
			</div>
 			
		</div>
		
		</div>
                
	</div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="clientsays_widget">
    
    	<div class="sidebar_title"><h4>Happy <i>Client Say's</i></h4></div>
        
        <img src="http://placehold.it/50x50" alt="" />
<strong>- Henry Brodie</strong><p>Lorem Ipsum passage, and going through the cites of the word here classical literature passage discovere there undou btable source looks reasonable the generated charac eristic words.</p>  
                
	</div><!-- end section -->
    
    <div class="clearfix margin_top4"></div>
   
    
    <div class="clearfix margin_top3"></div>
    
	<div class="sidebar_widget">
    
    	<div class="sidebar_title"><h4>Site <i>Advertisements</i></h4></div>
        
			<ul class="adsbanner-list">  
            	<li><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
                <li class="last"><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
            </ul>
                 
           	<ul class="adsbanner-list">  
            	<li><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
            	<li class="last"><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
           	</ul>
            
	</div><!-- end section -->
	
	<div class="clearfix margin_top4"></div>
    
	<div class="sidebar_widget">
    
    	<div class="sidebar_title"><h3>Site <i>Archives</i></h3></div>
        
		<ul class="arrows_list1">		
            <li><a href="#"><i class="fa fa-angle-right"></i> July 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> June 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> May 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> April 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> March 2014</a></li>
		</ul>
        
	</div><!-- end section -->
    
</div><!-- end right sidebar -->


</div><!-- end content area -->

<div class="clearfix margin_top7"></div>

<!-- footer -->
<jsp:include page="../Layout/footer.jsp"></jsp:include>
<!-- end footer -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->





</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- slide panel -->
<script type="text/javascript" src="../../js/slidepanel/slidepanel.js"></script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script> 
<script src="../../js/mainmenu/customeUI.js"></script> 

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- tab widget -->
<script type="text/javascript" src="../../js/tabs/tabwidget/tabwidget.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>

<script type="text/javascript" src="../../js/cubeportfolio/main4.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main6.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>



</body>
</html>
