<%@page import="Access.LinhVucAccess"%>
<%@page import="Model.LinhVucModel" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
    <meta charset="utf-8">
	<title>Trang Tin Tức</title>
	
	
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../../images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />
    
    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->
    
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="../../js/slidepanel/slidepanel.css">
    
	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/tabwidget/tabwidget.css" />

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
	
 
</head>

<body>


<div class="site_wrapper">


<header id="header">
	
	<div id="trueHeader">
    
	<div class="wrapper">
    
    
    <div class="logoarea">
    <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
    
	<div class="right_links">
        
        <ul>
            <li class="link"><a class="fancybox fancybox.ajax" href="login-frame.html"><i class="fa fa-envelope"></i> info@yourwebsite.com</a></li>
            <li class="link"><a class="fancybox fancybox.ajax" href="register-frame.html"><i class="fa fa-edit"></i> Đăng Nhập</a></li>
            <li class="social"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li class="social"><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
        
    </div><!-- end right links -->
    
    
    </div>
    </div>
		
	<!-- Menu -->
	<div class="menu_main">
    
    <div class="container">
        
	<div class="navbar yamm navbar-default">
    
    <div class="container">
      <div class="navbar-header">
        <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
          <button type="button" > <i class="fa fa-bars"></i></button>
        </div>
      </div>
      
      <div id="navbar-collapse-1" class="navbar-collapse collapse">
      
        <ul class="nav navbar-nav">
        
        <li><a href="index.html">Trang Chủ</a></li>
        
        <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Bảng Tin</a>
            <ul class="dropdown-menu" role="menu">
            <li><a href="http://gsrthemes.com/hoxa/layout1/fullwidth/index.html">Business</a> </li>
            <li><a href="http://gsrthemes.com/hoxa/fullwidth/index-1.html">Creative</a> </li>
            <li><a href="http://gsrthemes.com/hoxa/fullwidth/index-2.html">Portfolio</a> </li>
            <li><a href="http://gsrthemes.com/hoxa/layout2/index.html">One Page Theme</a> </li>
            <li><a href="http://gsrthemes.com/hoxa/fullwidth/index-3.html">Landing Page</a> </li>
            <li><a href="http://gsrthemes.com/hoxa/fullwidth/index.html">Corporate</a> </li>
            </ul>
        </li>
       
        <li class="dropdown yamm-fw"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Giới Thiệu</a>
        <ul class="dropdown-menu">
          <li> 
            <div class="yamm-content">
              <div class="row">
              
                <ul class="col-sm-6 col-md-3 list-unstyled ">
                  <li>
                    <p>Liên Hệ</p>
                  </li>
                  <li>
                  <img src="http://placehold.it/250x130" alt="" class="img_left4" />
                  Latin words, combined with an handfule model an sentence structures generate Lorem Ipsum which looks a reasonable. The generated Lorem Ipsum therefore always free on internet.</li>
                </ul>
                
                <ul class="col-sm-6 col-md-3 list-unstyled ">
                    <li>
                    <p>Useful Pages</p>
                    </li>
                    <li><a href="elements.html"><i class="fa fa-angle-right"></i> Elements</a></li>
                    <li><a href="typography.html"><i class="fa fa-angle-right"></i> Typography</a></li>
                    <li><a href="pricing-tables.html"><i class="fa fa-angle-right"></i> Pricing Tables</a></li>
                    <li><a href="columns.html"><i class="fa fa-angle-right"></i> Page Columns</a></li>
                    <li><a href="team.html"><i class="fa fa-angle-right"></i> Our Team</a></li>
                    <li><a href="faqs.html"><i class="fa fa-angle-right"></i> FAQs</a></li>
                    <li><a href="tabs.html"><i class="fa fa-angle-right"></i> Tabs</a></li>
                    <li><a href="login.html"><i class="fa fa-angle-right"></i> Login Form</a></li>
                    <li><a href="register.html"><i class="fa fa-angle-right"></i> Register Form</a></li>
                </ul>
                
                <ul class="col-sm-6 col-md-3 list-unstyled ">
                    <li>
                       <p>Diffrent Websites</p>
                    </li>
                    <li> <a href="http://gsrthemes.com/hoxa/layout1/fullwidth/index.html"><i class="fa fa-angle-right"></i> Business</a> </li>
                    <li> <a href="http://gsrthemes.com/hoxa/fullwidth/index-1.html"><i class="fa fa-angle-right"></i> Creative</a> </li>
                    <li> <a href="http://gsrthemes.com/hoxa/layout2/index.html"><i class="fa fa-angle-right"></i> One Page Theme</a> </li>
                    <li> <a href="http://gsrthemes.com/hoxa/fullwidth/index-3.html"><i class="fa fa-angle-right"></i> Landing Page</a> </li>
                    <li> <a href="http://gsrthemes.com/hoxa/fullwidth/index.html"><i class="fa fa-angle-right"></i> Corporate</a> </li>
                    <li> <a href="http://gsrthemes.com/hoxa/fullwidth/index-2.html"><i class="fa fa-angle-right"></i> Portfolio Page</a> </li>
                    <li> <a href="#"><i class="fa fa-angle-right"></i> Parallax Backgrounds</a> </li>
                    <li> <a href="#"><i class="fa fa-angle-right"></i> Background Videos</a> </li>
                    <li> <a href="#"><i class="fa fa-angle-right"></i> Create your Own</a> </li>
                </ul>
                
                <ul class="col-sm-6 col-md-3 list-unstyled ">
                <li>
                   <p>More Features</p>
                </li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Mega Menu</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Diffrent Websites</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Parallax Backgrounds</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Premium Sliders</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Diffrent Slide Shows</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Video BG Effects</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> 100+ Feature Sections</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Use for any Website</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Free Updates</a></li>
                </ul>
                
              </div>
            </div>
          </li>
        </ul>
        </li>
        
        <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Liên Hệ</a>
          <ul class="dropdown-menu multilevel" role="menu">
            <li><a href="about.html">About Page Style 1</a></li>
            <li><a href="about-2.html">About Page Style 2</a></li>
            <li><a href="about-3.html">About Page Style 3</a></li>
            <li><a href="team.html">Our Team</a></li>
            <li><a href="services.html">Services Style 1</a></li>
            <li><a href="services-2.html">Services Style 2</a></li>
            <li><a href="services-3.html">Services Style 3</a></li>
            <li><a href="full-width.html">Full Width Page</a></li>
            <li><a href="left-sidebar.html">Left Sidebar Page</a></li>
            <li><a href="right-sidebar.html">Right Sidebar Page</a></li>
            <li><a href="left-nav.html">Left Navigation</a></li>
            <li><a href="right-nav.html">Right Navigation</a></li>
            <li><a href="login.html">Login Form</a></li>
            <li><a href="register.html">Registration Form</a></li>
            <li><a href="404.html">404 Error Page</a></li>
          <li class="dropdown-submenu mul"> <a tabindex="-1" href="#">Sub Menu + </a>
            <ul class="dropdown-menu">
                <li><a href="#">Menu Item 1</a></li>
                <li><a href="#">Menu Item 2</a></li>
                <li><a href="#">Menu Item 3</a></li>
            </ul>
          </li>
                    </ul>
        </li>
        
        </ul>
        
        <div id="wrap">
          <form action="index.html" autocomplete="on">
          <input id="search" name="search" type="text" placeholder=""><input id="search_submit" value="search" type="submit">
          </form>
        </div>  
            
      </div>
      </div>
     </div>
     
	</div>
    </div><!-- end menu -->
        
	</div>
    
	</div>
    
</header>

<div class="clearfix"></div>

<div class="page_title2">
<div class="container">

    <div class="two_third">
    
    	<div class="title"><h1>TRANG TIN TỨC</h1></div>
        
        <div class="pagenation">&nbsp;<a href="index.html">Trang Chủ</a> <i>/</i> <a href="#">Bảng Tin</a> <i>/</i> Trang Tin Tức</div>
        
    </div>
    
    <div class="one_third last">
    
    	<div class="site-search-area">
        
    	<form method="get" id="site-searchform" action="blog.html">
        <div>
        <input class="input-text" name="s" id="s" value="Nhập Nội Dung Tìm Kiếm..." onFocus="if (this.value == 'Nhập Nội Dung Tìm Kiếm...') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Nhập Nội Dung Tìm Kiếm...';}" type="text" />
        <input id="searchsubmit" value="Search" type="submit" />
        </div>
        
        </form>
		</div><!-- end site search -->
        
    </div>
    
</div>
</div><!-- end page title --> 

<div class="clearfix"></div>

<div class="container">

	<div class="content_left">
	
    <div class="blog_post">	
        <div class="blog_postcontent">
        <div class="image_frame"><a href="#"><img src="../../images/tin1.jpg" alt="" /></a></div>
        <h3><a href="blog-post.html">Nhiều trang web vẫn còn rất mới</a></h3>
            <ul class="post_meta_links">
            	<li><a href="#" class="date">22 THÁNG 7 NĂM 2014</a></li>
                <li class="post_by"><i>Đăng Bởi:</i> <a href="#">Adam Harrison</a></li>
                <li class="post_categoty"><i>Tại Lĩnh Vực:</i> <a href="#">Web tutorials</a></li>
                <li class="post_comments"><i>Nhận Xét:</i> <a href="#">18 Nhận Xét</a></li>
            </ul>
         <div class="clearfix"></div>
         <div class="margin_top1"></div>
        <p> Lorem Ipsum là nó có phân phối bình thường nhiều hơn hoặc ít hơn các chữ cái, trái với việc sử dụng 'Nội dung ở đây, nội dung ở đây', làm cho nó trông giống như tiếng Anh dễ đọc. Nhiều gói xuất bản dành cho máy tính để bàn và các trình soạn thảo trang web giờ đây đã sử dụng Lorem Ipsum làm mô hình mặc định của họ, và tìm kiếm cho 'lorem ipsum' sẽ khám phá nhiều trang web vẫn còn rất mới mẻ. Các phiên bản khác nhau đã được phát triển trong nhiều năm qua <a href="#"> Đọc Tiếp...</a></p>
        </div>
    </div><!-- /# end post -->
    
    <div class="clearfix divider_dashed9"></div>
    
    <div class="blog_post">	
        <div class="blog_postcontent">
        <div class="image_frame"><a href="#"><img src="../../images/tin2.jpg" alt="" /></a></div>
        <h3><a href="blog-post.html">Các tập phiếu Letraset chứa đoạn văn lorem</a></h3>
            <ul class="post_meta_links">
            	<li><a href="#" class="date">21 THÁNG 7 NĂM 2014</a></li>
                <li class="post_by"><i>Đăng Bởi:</i> <a href="#">Adam Harrison</a></li>
                <li class="post_categoty"><i>Tại Lĩnh Vực:</i> <a href="#">Web tutorials</a></li>
                <li class="post_comments"><i>Nhận Xét:</i> <a href="#">18 Nhận Xét</a></li>
            </ul>
         <div class="clearfix"></div>
         <div class="margin_top1"></div>
        <p>Lorem Ipsum là nó có phân phối bình thường nhiều hơn hoặc ít hơn các chữ cái, trái với việc sử dụng 'Nội dung ở đây, nội dung ở đây', làm cho nó trông giống như tiếng Anh dễ đọc. Nhiều gói xuất bản dành cho máy tính để bàn và các trình soạn thảo trang web giờ đây đã sử dụng Lorem Ipsum làm mô hình mặc định của họ, và tìm kiếm cho 'lorem ipsum' sẽ khám phá nhiều trang web vẫn còn rất mới mẻ. Các phiên bản khác nhau đã được phát triển trong nhiều năm qua <a href="#"> Đọc Tiếp...</a></p>
        </div>
    </div><!-- /# end post -->
    
    <div class="clearfix divider_dashed9"></div>
    
    
    <div class="blog_post">	
        <div class="blog_postcontent">
        <div class="video_frame"><iframe src="http://www.youtube.com/embed/YRb-xF0RW-k"></iframe></div>
        <h3><a href="blog-post.html">Thông qua các trích dẫn từ trong văn học cổ điển</a></h3>
            <ul class="post_meta_links">
            	<li><a href="#" class="date">20 THÁNG 7 NĂM 2014</a></li>
                <li class="post_by"><i>Đăng Bởi:</i> <a href="#">Adam Harrison</a></li>
                <li class="post_categoty"><i>Tại Lĩnh Vực:</i> <a href="#">Web tutorials</a></li>
                <li class="post_comments"><i>Nhận Xét:</i> <a href="#">18 Nhận Xét</a></li>
            </ul>
         <div class="clearfix"></div>
         <div class="margin_top1"></div>
        <p>Lorem Ipsum là nó có phân phối bình thường nhiều hơn hoặc ít hơn các chữ cái, trái với việc sử dụng 'Nội dung ở đây, nội dung ở đây', làm cho nó trông giống như tiếng Anh dễ đọc. Nhiều gói xuất bản dành cho máy tính để bàn và các trình soạn thảo trang web giờ đây đã sử dụng Lorem Ipsum làm mô hình mặc định của họ, và tìm kiếm cho 'lorem ipsum' sẽ khám phá nhiều trang web vẫn còn rất mới mẻ. Các phiên bản khác nhau đã được phát triển trong nhiều năm qua <a href="#"> Đọc Tiếp...</a></p>
        </div>
    </div><!-- /# end post -->
    
    <div class="clearfix divider_dashed9"></div>
    
    
    <div class="blog_post">	
        <div class="blog_postcontent">
        <div class="image_frame"><a href="#"><img src="../../images/tin3.jpg" alt="" /></a></div>
        <h3><a href="blog-post.html">Sự bùng nổ về lĩnh vực Internet</a></h3>
            <ul class="post_meta_links">
            	<li><a href="#" class="date">19 THÁNG 7 NĂM 2014</a></li>
                <li class="post_by"><i>Đăng Bởi:</i> <a href="#">Adam Harrison</a></li>
                <li class="post_categoty"><i>Tại Lĩnh Vực:</i> <a href="#">Web tutorials</a></li>
                <li class="post_comments"><i>Nhận Xét:</i> <a href="#">18 Nhận Xét</a></li>
            </ul>
         <div class="clearfix"></div>
         <div class="margin_top1"></div>
        <p>Lorem Ipsum là nó có phân phối bình thường nhiều hơn hoặc ít hơn các chữ cái, trái với việc sử dụng 'Nội dung ở đây, nội dung ở đây', làm cho nó trông giống như tiếng Anh dễ đọc. Nhiều gói xuất bản dành cho máy tính để bàn và các trình soạn thảo trang web giờ đây đã sử dụng Lorem Ipsum làm mô hình mặc định của họ, và tìm kiếm cho 'lorem ipsum' sẽ khám phá nhiều trang web vẫn còn rất mới mẻ. Các phiên bản khác nhau đã được phát triển trong nhiều năm qua <a href="#"> Đọc Tiếp...</a></p>
        </div>
    </div><!-- /# end post -->
    
    <div class="clearfix divider_dashed9"></div>
    
    
    <div class="blog_post">	
        <div class="blog_postcontent">
        <div class="image_frame"><a href="#"><img src="../../images/tin4.jpg" alt="" /></a></div>
        <h3><a href="blog-post.html">Các phiên bản đã phát triển nhiều năm qua</a></h3>
            <ul class="post_meta_links">
            	<li><a href="#" class="date">18 THÁNG 7 NĂM 2014</a></li>
                <li class="post_by"><i>Đăng Bởi:</i> <a href="#">Adam Harrison</a></li>
                <li class="post_categoty"><i>Tại Lĩnh Vực:</i> <a href="#">Web tutorials</a></li>
                <li class="post_comments"><i>Nhận Xét:</i> <a href="#">18 Nhận Xét</a></li>
            </ul>
         <div class="clearfix"></div>
         <div class="margin_top1"></div>
        <p>Lorem Ipsum là nó có phân phối bình thường nhiều hơn hoặc ít hơn các chữ cái, trái với việc sử dụng 'Nội dung ở đây, nội dung ở đây', làm cho nó trông giống như tiếng Anh dễ đọc. Nhiều gói xuất bản dành cho máy tính để bàn và các trình soạn thảo trang web giờ đây đã sử dụng Lorem Ipsum làm mô hình mặc định của họ, và tìm kiếm cho 'lorem ipsum' sẽ khám phá nhiều trang web vẫn còn rất mới mẻ. Các phiên bản khác nhau đã được phát triển trong nhiều năm qua <a href="#"> Đọc Tiếp...</a></p>
        </div>
    </div><!-- /# end post -->
    
    <div class="clearfix divider_dashed9"></div>
    
	<div class="pagination">
    <b>Trang 2 của 18</b>
    <a href="#" class="navlinks">&lt; Quay Về</a>
        <a href="#" class="navlinks">1</a>
        <a href="#" class="navlinks current">2</a>
        <a href="#" class="navlinks">3</a>
        <a href="#" class="navlinks">4</a>
        <a href="#" class="navlinks">Tiếp Theo</a>
    </div><!-- /# end pagination -->


</div><!-- end content left side -->

<!-- right sidebar starts -->
<div class="right_sidebar">
    
    <div class="sidebar_widget">
        <%
            LinhVucAccess linhvuc = new LinhVucAccess();
        %>
    
    	<div class="sidebar_title"><h4>CÁC <i>LĨNH VỰC</i></h4></div>
		<ul class="arrows_list1">	
                    <%
                              for(LinhVucModel ds : linhvuc.getListLinhVuc())
                              {
                              %>
                              <li><a href="#"><i class="fa fa-caret-right"></i> <%=ds.getTenLV()%></a></li>
                           <%
                               }
                           %>
		</ul>
        
	</div><!-- end section -->
    
    <div class="clearfix margin_top4"></div>
    
    <div class="sidebar_widget">
    
    	<div id="tabs">
        
			<ul class="tabs">  
				<li class="active"><a href="#tab1">PHỔ BIẾN</a></li>
				<li><a href="#tab2">GẦN ĐÂY</a></li>
				<li class="last"><a href="#tab3">THẺ</a></li>
			</ul><!-- /# end tab links --> 
 
		<div class="tab_container">	
			<div id="tab1" class="tab_content"> 
            
				<ul class="recent_posts_list">
					<li>
					  	<span><a href="#"><img src="../../images/tin5.jpg" alt="" /></a></span>
                                                <a href="#">Đang xuất bản trang Web Publishing packag</a>
						 <i>13 tháng 7 năm 2014</i> 
					</li>	
					<li>
					  	<span><a href="#"><img src="../../images/tin6.jpg" alt="" /></a></span>
						<a href="#">Sublishing packag esanse web page editos</a>
						 <i>12 tháng 7 năm 2014</i> 
					</li>	
					<li class="last">
					  	<span><a href="#"><img src="../../images/tin7.jpg" alt="" /></a></span>
						<a href="#">Mublishing packag esanse web page editos</a>
						 <i>11 tháng 7 năm 2014</i> 
					</li>
				</ul>
                 
			</div><!-- end popular posts --> 
			
			<div id="tab2" class="tab_content">	 
				<ul class="recent_posts_list">
                
					<li>
					  	<span><a href="#"><img src="../../images/tin8.jpg" alt="" /></a></span>
						<a href="#">Various versions has evolved over the years</a>
						 <i>18 tháng 7 năm 2014</i> 
					</li>
					<li>
					  	<span><a href="#"><img src="../../images/tin9.jpg" alt="" /></a></span>
						<a href="#">Rarious versions has evolve over the years</a>
						 <i>17 tháng 7 năm 2014</i> 
					</li>
					<li class="last">
					  	<span><a href="#"><img src="../../images/tin10.jpg" alt="" /></a></span>
						<a href="#">Marious versions has evolven over the years</a>
						 <i>16 tháng 7 năm 2014</i> 
					</li>
				</ul>
                 
			</div><!-- end popular articles -->	
			
			<div id="tab3" class="tab_content">	 
				<ul class="tags">
														
					<li><a href="#">2014</a></li>
					<li><a href="#"><b>Amazing</b></a></li>
					<li><a href="#">Animation</a></li>
					<li><a href="#">Beautiful</a></li>
					<li><a href="#"><b>Cartoon</b></a></li>
					<li><a href="#">Comedy</a></li>
					<li><a href="#"><b>Cool</b></a></li>
					<li><a href="#">Dance</a></li>
					<li><a href="#">Drive</a></li>
					<li><a href="#"><b>Family</b></a></li>
					<li><a href="#"><b>Fantasy</b></a></li>
					<li><a href="#">News</a></li>
					<li><a href="#"><b>Friends</b></a></li>
					<li><a href="#">Funny</a></li>
					<li><a href="#"><b>Games</b></a></li>
					<li><a href="#">Love</a></li>
					<li><a href="#"><b>Music</b></a></li>
					<li><a href="#">Nature</a></li>
					<li><a href="#"><b>Party</b></a></li>
					<li><a href="#">Pictures</a></li>
					<li><a href="#">Sports</a></li>
					<li><a href="#"><b>Best</b></a></li>
					<li><a href="#">Wedding</a></li>
					<li><a href="#">Weight</a></li>
					<li><a href="#"><b>Youtube</b></a></li>
				</ul>	 
			</div>
 			
		</div>
		
		</div>
                
	</div><!-- end section -->
    
    <div class="clearfix margin_top5"></div>
    
    <div class="clientsays_widget">
    
    	<div class="sidebar_title"><h4>Happy <i>Client Say's</i></h4></div>
        
        <img src="../../images/tin11.jpg" alt="" />
<strong>- Henry Brodie</strong><p>Lorem Ipsum passage, và đi qua các trích dẫn của từ ở đây văn học cổ điển passage khám phá ra rằng có undouch btable nguồn có vẻ hợp lý các từ charistic tạo ra charac.</p>  
                
	</div><!-- end section -->
    
    <div class="clearfix margin_top4"></div>
    
    <div class="sidebar_widget">
    
    	<div class="sidebar_title"><h4>Danh mục đầu tư <i>Widget</i></h4></div>
        
        <div class="features_sec15 two">
                
            <div>
              <section class="slider nosidearrows">
                <div class="flexslider carousel">
                
                  <ul class="slides">
            
                        <li>
                        
                            <div class="grid-container cbp-l-grid-fullScreen sidebar">
                            <ul>
                              <li class="cbp-item identity logo"> <a href="../../images/tin12.jpg" class="cbp-caption cbp-lightbox" data-title="Consectetuer Adipiscing<br>by Rutrum adipiscing luctus">
                                <div class="cbp-caption-defaultWrap"> <img src="../../images/tin13.jpg" alt="" /> </div>
                                <div class="cbp-caption-activeWrap">
                                  <div class="cbp-l-caption-alignLeft">
                                    <div class="cbp-l-caption-body">
                                      <div class="margin_top7"></div>
                                      <div class="cbp-l-caption-title">Thông Tin</div>
                                      <div class="cbp-l-caption-desc">Thông Tin</div>
                                      </div>
                                  </div>
                                </div>
                                </a>
                                </li>
							</ul>
                            </div>
                            
                        </li><!-- end section -->
                        
                        <li>
                        
                            <div class="grid-container cbp-l-grid-fullScreen sidebar">
                            <ul>
                              <li class="cbp-item"> <a href="../../images/tin12.jpg" class="cbp-caption cbp-lightbox" data-title="Many Websites Infancy<br>by Rutrum adipiscing luctus">
                                <div class="cbp-caption-defaultWrap"> <img src="../../images/tin13.jpg" alt="" /> </div>
                                <div class="cbp-caption-activeWrap">
                                  <div class="cbp-l-caption-alignLeft">
                                    <div class="cbp-l-caption-body">
                                    <div class="margin_top7"></div>
                                      <div class="cbp-l-caption-title">Thông Tin</div>
                                      <div class="cbp-l-caption-desc">Thông Tin</div>
                                    </div>
                                  </div>
                                </div>
                                </a>
                                </li>
							</ul>
                            </div>
                            
                        </li><!-- end section -->
                        
                        
						<li>
                        
                            <div class="grid-container cbp-l-grid-fullScreen sidebar">
                            <ul>
                              <li class="cbp-item motion graphic"> <a href="../../images/tin12.jpg" class="cbp-caption cbp-lightbox" data-title="Words Believable<br>by Rutrum adipiscing luctus">
                                <div class="cbp-caption-defaultWrap"> <img src="../../images/tin13.jpg" alt="" /> </div>
                                <div class="cbp-caption-activeWrap">
                                  <div class="cbp-l-caption-alignLeft">
                                    <div class="cbp-l-caption-body">
                                    <div class="margin_top7"></div>
                                      <div class="cbp-l-caption-title">Thông Tin</div>
                                      <div class="cbp-l-caption-desc">Thông Tin</div>
                                    </div>
                                  </div>
                                </div>
                                </a>
                                </li>
							</ul>
                            </div>
                            
                        </li><!-- end section -->
                              
                  </ul>
                </div>
                <div class="cbp-l-loadMore-text">
                    <div data-href="#" class="cbp-l-loadMore-text-link"></div>
                </div>
                
              </section>
              
            </div>
             
        </div>
               
	</div><!-- end section -->
    
    <div class="clearfix margin_top3"></div>
    
	<div class="sidebar_widget">
    
    	<div class="sidebar_title"><h4>Trang Web <i>Quảng Cáo</i></h4></div>
        
			<ul class="adsbanner-list">  
            	<li><a href="#"><img src="images/sample-ad-banner.jpg" alt="" /></a></li>
                <li class="last"><a href="#"><img src="images/sample-ad-banner.jpg" alt="" /></a></li>
            </ul>
                 
           	<ul class="adsbanner-list">  
            	<li><a href="#"><img src="images/sample-ad-banner.jpg" alt="" /></a></li>
            	<li class="last"><a href="#"><img src="images/sample-ad-banner.jpg" alt="" /></a></li>
           	</ul>
            
	</div><!-- end section -->
	
	<div class="clearfix margin_top4"></div>
    
	<div class="sidebar_widget">
    
    	<div class="sidebar_title"><h3>Tin Tức <i>Lưu Trữ</i></h3></div>
        
		<ul class="arrows_list1">		
            <li><a href="#"><i class="fa fa-angle-right"></i> Tháng 7 năm 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Tháng 6 năm 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Tháng 5 năm 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Tháng 4 năm 2014</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Tháng 3 năm 2014</a></li>
		</ul>
        
	</div><!-- end section -->
    
</div><!-- end right sidebar -->


</div><!-- end content area -->

<div class="clearfix margin_top3"></div>


        <jsp:include page="../Layout/footer.jsp"></jsp:include>

    
</div>
</div><!-- end copyright info -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->





</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- slide panel -->
<script type="text/javascript" src="../../js/slidepanel/slidepanel.js"></script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script> 
<script src="../../js/mainmenu/customeUI.js"></script> 

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- tab widget -->
<script type="text/javascript" src="../../js/tabs/tabwidget/tabwidget.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>

<script type="text/javascript" src="../../js/cubeportfolio/main4.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main6.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>



</body>
</html>
