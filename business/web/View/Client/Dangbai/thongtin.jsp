<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        
         <link rel='shortcut icon' href='https://cdn2.iconfinder.com/data/icons/agriculture-1/512/care-512.png'/>
	<title>Quản Lý Bài Đăng Client</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../../images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />

    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->
    
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
	<!-- Master Slider -->
    <link rel="stylesheet" href="../../js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="../../js/masterslider/skins/default/style.css" />
    <link rel="stylesheet" href="../../js/masterslider/style.css" />
    
    <link href='../../js/masterslider/style/ms-staff-style.css' rel='stylesheet' type='text/css'>

	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs2.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs3.css">

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
    </head>

    <body>
        <div class="site_wrapper">

        <jsp:include page="../Layout/header.jsp"></jsp:include>
        <div class="page_title2">
<div class="container">

    <div class="title"><h1>Quản Lý Thông Tin Bài Đăng</h1></div>
    
    <div class="pagenation">&nbsp;<a href="index.html">Trang Chủ</a> <i>/</i> Quản Lý Bài Đăng</div>
    
</div>
</div><!-- end page title --> 


<div class="clearfix"></div>


<!-- MENU search -->
<div class="container">
    
        
        <br />
        
       
			

</div><!-- End menu search -->

<div class="clearfix"></div>
  



<div class="features_sec42 two">
    <div class="container">
    <div class="title"><h3>Những Bài Đăng Đã Đăng</h3></div>
        
        <br />
        
        <div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>John Frances</h5>
            <h6>Manager</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
            <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Lucia Penelope</h5>
            <h6>Development</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
            <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Mark Adraison</h5>
            <h6>Design</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
              <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth last animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Anya Siennadia</h5>
            <h6>Support</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
             <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
    </div>
    </div><!-- end team section -->
    
   
    
    
    <div class="clearfix divider_dashed2"></div><!-- end divider line -->
  
   
    <div class="features_sec42 two">
    <div class="container">
   <div class="title"><h3>Những Bài Đăng Chưa Được Kiểm Duyệt</h3></div>
        
        <br />
    
		<div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Hendrie Phil</h5>
            <h6>Support</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
             <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Fred Jesse</h5>
            <h6>Marketing</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
             <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Krieger Robbie</h5>
            <h6>Support</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
              <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
        <div class="one_fourth last animate" data-anim-type="fadeInUp">
        <div class="box">
            
            <img src="http://placehold.it/270x245" alt="" class="rimg" />
            
            <h5>Richard John</h5>
            <h6>Marketing</h6>
            
            <p>Lorem ipsum dolor amet, consectetue Radipiscing elit et Suspendisse et justo. mattis commodo augue. Aliquam ornare hendrerit augue cars.</p>
            <br />
              <button type="submit" class="btn btn-primary">Xem Bài Đăng</button>   <button type="submit" class="btn btn-primary">Sửa Bài Đăng</button>
            
        </div>
        </div><!-- end section -->
        
    </div>
    </div><!-- end team section -->

  
</div>
</div><!-- end content area -->

<div class="clearfix margin_top5"></div>




        
        <jsp:include page="../Layout/footer.jsp"></jsp:include>
        </div>
        
        
        
    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- Master Slider -->
<script src="../../js/masterslider/jquery.easing.min.js"></script>
<script src="../../js/masterslider/masterslider.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";

var slider = new MasterSlider();
 slider.setup('masterslider' , {
     width: 1400,    // slider standard width
     height:720,   // slider standard height
     space:0,
	 speed:45,
     fullwidth:true,
     loop:true,
     preload:0,
     autoplay:true,
	 view:"basic"
});
// adds Arrows navigation control to the slider.
slider.control('arrows');
slider.control('bullets');

})(jQuery);
</script>

<script type="text/javascript">	
(function($) {
 "use strict";

	var slider5 = new MasterSlider();
	slider5.setup('masterslider5' , {
		loop:true,
		width:240,
		height:240,
		speed:20,
		view:'focus',
		preload:0,
		space:35,
		viewOptions:{centerSpace:1.6}
	});
	slider5.control('arrows');
	slider5.control('slideinfo',{insertTo:'#staff-info'});
	
})(jQuery);
</script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>

    </body>
</html>
