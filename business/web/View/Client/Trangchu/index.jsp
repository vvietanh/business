<%@page import="Access.VonAccess"%>
<%@page import="Model.VonModel" %>
<%@page import="Access.LinhVucAccess"%>
<%@page import="Model.LinhVucModel" %>
<%@page import="Access.KhuVucAccess"%>
<%@page import="Model.KhuVucModel" %>
<%@page import="Access.BaiDangAccess" %>
<%@page import="Model.BaiDangModel" %>
<%@page import="Access.DoanhNghiepAccess" %>
<%@page import="Model.DoanhNghiepModel" %>
<%@page import="Access.HinhDoanhNghiepAccess" %>
<%@page import="Model.HinhDoanhNghiepModel" %>
<%@page import="Access.ChiTietDoanhNghiepAccess" %>
<%@page import="Model.ChiTietDoanhNghiepModel" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        
         <link rel='shortcut icon' href='https://cdn2.iconfinder.com/data/icons/agriculture-1/512/care-512.png'/>
	<title>Start-Up Việt Nam</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../../images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />

    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
	<!-- Master Slider -->
    <link rel="stylesheet" href="../../js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="../../js/masterslider/skins/default/style.css" />
    <link rel="stylesheet" href="../../js/masterslider/style.css" />
    
    <link href='../../js/masterslider/style/ms-staff-style.css' rel='stylesheet' type='text/css'>

	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs2.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs3.css">

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
    </head>

    <body>
        <div class="site_wrapper">

        <jsp:include page="../Layout/header.jsp"></jsp:include>
        
            
<!-- Slider
======================================= -->  

<div class="container_full slidertop">
    
    
    <div class="master-slider ms-skin-default" id="masterslider">
    
    <!-- slide -->
            <div class="ms-slide slide-1" data-delay="8">
                <!-- slide background -->
                <img src="../../images/3.jpg" alt=""/>
            </div>
    <!-- end of slide -->
</div>

</div><!-- end slider -->
<div class="clearfix"></div>


<!-- MENU search -->
<div class="container">

        <br />
        
        <%
            LinhVucAccess linhvuc = new LinhVucAccess();
            KhuVucAccess khuvuc = new KhuVucAccess();
            VonAccess von = new VonAccess();
            BaiDangAccess bd = new BaiDangAccess();
            DoanhNghiepAccess dn = new DoanhNghiepAccess();
            HinhDoanhNghiepAccess hdn = new HinhDoanhNghiepAccess();
            ChiTietDoanhNghiepAccess ctdn = new ChiTietDoanhNghiepAccess();
        %>
       
	<div class="row">
                <div class="container" style="margin-left:30px; ">
                    <div class="form-group" style="float:left;">
                        <select class="form-control" id="exampleSelect1">
                          <option value="0" selected disabled>Lĩnh Vực</option>
                          <%
                              for(LinhVucModel ds : linhvuc.getListLinhVuc())
                              {
                              %>
                              <option><%=ds.getTenLV()%></option>
                           <%
                               }
                           %>
                         
                        </select>
                     </div>
                
                    <div class="form-group" style="float:left;padding-left:10px;">
                        <select class="form-control" id="exampleSelect1">
                          <option value="0" selected disabled>Khu Vực</option>
                          <%
                              for(KhuVucModel ds : khuvuc.getListKhuVuc())
                              {
                              %>
                              <option><%=ds.getTenKV()%></option>
                           <%
                               }
                           %>
                        </select>
                     </div>
        
                    <div class="form-group" style="float:left;padding-left:10px;">
                        <select class="form-control" id="exampleSelect1">
                          <option value="0" selected disabled>Vốn</option>
                          <%
                              for(VonModel ds : von.getListVon())
                              {
                              %>
                              <option><%=ds.getMucVon()%> <%=ds.getDonVi()%></option>
                           <%
                               }
                           %>
                        </select>
                     </div>
                
                
                      <div class="form-group" style="float:left;padding-left:10px;">
                        <input type="text" class="form-control" placeholder="Search">
                      </div>
                      <button type="submit" class="btn btn-success">Submit</button>
				</div>
         
    </div>

</div><!-- End menu search -->

<div class="clearfix"></div>



<div class="features_sec42 two">
    <div class="container">
        <br />
        <%
            for(DoanhNghiepModel ds : dn.getListDN())
            {
        %>
            <div class="one_fourth animate" data-anim-type="fadeInUp">
            <div class="box">
                <%
                    HinhDoanhNghiepModel md = hdn.getHinhByMaDN(ds.getMaDN());
                    BaiDangModel mdbd = bd.getBDByIdBD(ds.getMaDN());
                    KhuVucModel mdkv = khuvuc.getKhuVucByIdKV(ds.getMaKV());
                    ChiTietDoanhNghiepModel ctdnmd = ctdn.getLinhVucByIdLV(ds.getMaDN());
                    LinhVucModel mdlv = linhvuc.getLinhVucByIdLV(ctdnmd.getMaLV());

                 %>
                <img src="<%= md.getDuongDan() %>" alt="" class="rimg" />   
               
                <p style="color:#80ba0c; font-size:16px;"><a href="${root}/business/View/Client/Baidang/chitietbaidang.jsp?MaBD=<%=mdbd.getMaBD()%>"><%=ds.getTenDN()%></a></p>
                <h6> <i>Ngày đăng: <b><%= mdbd.getNgayDang()%></b></i></h6>
                <h6> <i>Khu vực: <b><%= mdkv.getTenKV() %></b> </i></h6>
                <h6> <i>Lĩnh vực: <b><%= mdlv.getTenLV() %></b> </i></h6>

                <p><%= mdbd.getMoTa() %></p>
                <br />
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>

            </div>
            </div><!-- end section -->
        
         <%
             }
         %>
        
    </div>
</div><!-- end content area -->

<div class="clearfix margin_top5"></div>

        <jsp:include page="../Layout/footer.jsp"></jsp:include>
        
        
    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- Master Slider -->
<script src="../../js/masterslider/jquery.easing.min.js"></script>
<script src="../../js/masterslider/masterslider.min.js"></script>
<script type="text/javascript">
(function($) {
 "use strict";

var slider = new MasterSlider();
 slider.setup('masterslider' , {
     width: 1400,    // slider standard width
     height:720,   // slider standard height
     space:0,
	 speed:45,
     fullwidth:true,
     loop:true,
     preload:0,
     autoplay:true,
	 view:"basic"
});
// adds Arrows navigation control to the slider.
slider.control('arrows');
slider.control('bullets');

})(jQuery);
</script>

<script type="text/javascript">	
(function($) {
 "use strict";

	var slider5 = new MasterSlider();
	slider5.setup('masterslider5' , {
		loop:true,
		width:240,
		height:240,
		speed:20,
		view:'focus',
		preload:0,
		space:35,
		viewOptions:{centerSpace:1.6}
	});
	slider5.control('arrows');
	slider5.control('slideinfo',{insertTo:'#staff-info'});
	
})(jQuery);
</script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>

    </body>
</html>