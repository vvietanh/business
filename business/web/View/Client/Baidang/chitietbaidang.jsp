<%@page import="Model.HinhDoanhNghiepModel"%>
<%@page import="Access.HinhDoanhNghiepAccess"%>
<%@page import="Access.UserAccess"%>
<%@page import="Model.TaiKhoanModel"%>
<%@page import="Access.DoanhNghiepAccess"%>
<%@page import="Model.DoanhNghiepModel"%>
<%@page import="Model.HinhBaiDangModel"%>
<%@page import="Access.VonAccess"%>
<%@page import="Access.KhuVucAccess"%>
<%@page import="Access.LinhVucAccess"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="Model.BaiDangModel"%>
<%@page import="Access.BaiDangAccess"%>
﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

    <head>
        <link rel='shortcut icon' href='https://cdn2.iconfinder.com/data/icons/agriculture-1/512/care-512.png'/>
        <title>Chi Tiết Bảng Tin</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <!-- Favicon --> 
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- this styles only adds some repairs on idevices  -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Google fonts - witch you want to use - (rest you can just remove) -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
                    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->

        <!-- ######### CSS STYLES ######### -->

        <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
        <link rel="stylesheet" href="../../css/style.css" type="text/css" />

        <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">

        <!-- responsive devices styles -->
        <link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />

        <!-- animations -->
        <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

        <!-- mega menu -->
        <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/mainmenu/demo.css" rel="stylesheet" type="text/css"/>
        <link href="../../js/mainmenu/menu.css" rel="stylesheet" type="text/css"/>

        <!-- slide panel -->
        <link href="../../js/slidepanel/slidepanel.css" rel="stylesheet" type="text/css"/>

        <!-- cubeportfolio -->
        <link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">

        <!-- tabs -->
        <link rel="stylesheet" type="text/css" href="../../js/tabs/tabwidget/tabwidget.css" />

        <!-- carousel -->
        <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />

        <!-- accordion -->
        <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">

        <!-- Lightbox -->
        <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />


    </head>

    <body>
        <%
            LinhVucAccess linhvuc = new LinhVucAccess();
            KhuVucAccess khuvuc = new KhuVucAccess();
            VonAccess von = new VonAccess();
            BaiDangAccess bdaccess = new BaiDangAccess();
            BaiDangModel bdmodel = new BaiDangModel();
            DoanhNghiepModel dn = new DoanhNghiepModel();
            DoanhNghiepAccess dnaccess = new DoanhNghiepAccess();
            TaiKhoanModel tk = new TaiKhoanModel();
            HinhDoanhNghiepAccess hinhdn = new HinhDoanhNghiepAccess();
            HinhDoanhNghiepModel hinhdnmd = new HinhDoanhNghiepModel();
            String MaBD = "";
            if (request.getParameter("MaBD") != null) {
                MaBD = request.getParameter("MaBD");
                bdmodel = bdaccess.getBDByIdBD(Integer.parseInt(MaBD));
                dn = dnaccess.getDNbyidBD(Integer.parseInt(MaBD));

            }

        %>


        <div class="site_wrapper">

            <jsp:include page="../Layout/header.jsp"></jsp:include>

                <div class="page_title2">


                    <div class="container">

                        <div class="two_third">

                            <div class="title"><h1><%=bdmodel.getTieuDe()%></h1></div>

                        <div class="pagenation">&nbsp;<a href="index.html">Home</a> <i>/</i> <a href="#">Blog</a> <i>/</i> Single Post</div>

                    </div>

                    <div class="one_third last">

                        <div class="site-search-area">

                            <form method="get" id="site-searchform" action="blog.html">
                                <div>
                                    <input class="input-text" name="s" id="s" value="Nhập từ khóa tìm kiếm" onFocus="if (this.value == 'Enter Search Blog...') {
                                                this.value = '';
                                            }" onBlur="if (this.value == '') {
                                                        this.value = 'Enter Search Blog...';
                                                    }" type="text" />
                                    <input id="searchsubmit" value="Search" type="submit" />
                                </div>

                            </form>
                        </div><!-- end site search -->

                    </div>

                </div>
            </div><!-- end page title --> 

            <div class="clearfix"></div>

            <div class="container">
                <div class="content_left">

                    <div class="blog_post">	
                        <div class="blog_postcontent">
                            <%
                                HinhBaiDangModel hinhbd = bdaccess.getHinhByMaBD(bdmodel.getMaBD());
                                UserAccess user = new UserAccess();
                                tk = user.getPosterbyID(bdmodel.getNguoiDang());
                            %>
                            <div class="image_frame"><a href="#"><img src="<%=hinhbd.getDuongDan()%>" alt=""/></a></div>
                            <h3><a href="blog-post.html"><%=dn.getTenDN()%></a></h3>
                            <ul class="post_meta_links">
                                <li><a href="#" class="date"><%=bdmodel.getNgayDang()%></a></li>
                                <li class="post_by"><i>by:</i> <a href="#"><%=tk.getHoTen()%></a></li>
                                <!--                                <li class="post_categoty"><i>in:</i> <a href="#">Web tutorials</a></li>
                                                                <li class="post_comments"><i>note:</i> <a href="#">18 Bình luận</a></li>-->
                            </ul>
                            <div class="clearfix"></div>
                            <div class="margin_top1"></div>
                            <p><%=bdmodel.getNoiDung()%></p>
                        </div>
                    </div><!-- /# end post -->

                    <div class="clearfix divider_dashed9"></div>

                    <!--Trình chiếu ảnh-->
                    <div class="features_sec15 two">

                        <div>
                            <section class="slider nosidearrows">
                                <div class="flexslider">


                                    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 600%; transition-duration: 0s; transform: translate3d(-596px, 0px, 0px);">
                                            <%
                                                int count = 0;
                                                for (HinhDoanhNghiepModel images : hinhdn.getListHinhDN(dn.getMaDN())) {
                                            %>
                                            <li style="width: 800px; float: left; display: block;">

                                                <div class="cbp-l-grid-fullScreen sidebar cbp cbp-animation-bounceTop cbp-ready" style="height: 400px;">
                                                    <ul class="cbp-wrapper cbp-wrapper-front">
                                                        <li class="cbp-item identity logo" style="width: 800px; height: 400px; transform: translate3d(0px, 0px, 0px); opacity: 1;"><div class="cbp-item-wrapper"> <a href="http://placehold.it/800x600" class="cbp-caption cbp-lightbox" data-title="Consectetuer Adipiscing<br>by Rutrum adipiscing luctus">
                                                                    <div class="cbp-caption-defaultWrap"> <img src="<%=images.getDuongDan()%>" alt="" draggable="false"> </div>
                                                                </a>
                                                            </div></li>
                                                    </ul>
                                            </li><!-- end section -->
                                            <%
                                                    count++;
                                                }
                                                for (int i = 1; i <= count; i++) {
                                            %>

                                        </ul></div><ol class="flex-control-nav flex-control-paging">
                                        <li><a class=""><%=i%></a></li>
                                            <%
                                                }
                                            %>

                                </div>
                                <div class="cbp-l-loadMore-text">
                                    <div data-href="#" class="cbp-l-loadMore-text-link"></div>
                                </div>

                            </section>

                        </div>

                    </div>
                    <div class="clearfix divider_dashed9"></div>

                    <!--Chia sẻ bài viết-->
                    <div class="sharepost">
                        <h4>Chia sẻ bài viết này</h4>
                        <ul>
                            <li><a href="#">&nbsp;<i class="fa fa-facebook fa-lg"></i>&nbsp;</a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg"></i></a></li>
                        </ul>

                    </div><!-- end share post links -->

                    <div class="clearfix"></div>
                    <h4>Bình luận</h4>
                    <div class="mar_top_bottom_lines_small3"></div>
                    <div class="comment_wrap">
                        <div class="gravatar"><img src="../../images/blog/people_img.jpg" alt="" /></div>
                        <div class="comment_content">
                            <div class="comment_meta">

                                <div class="comment_author">Admin - <i>May 12, 2014</i></div>                   

                            </div>
                            <div class="comment_text">
                                <p>Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage.</p>
                                <a href="#">Reply</a>
                            </div>
                        </div>
                    </div><!-- end section -->

                    <div class="comment_wrap chaild">
                        <div class="gravatar"><img src="images/blog/people_img.jpg" alt="" /></div>
                        <div class="comment_content">
                            <div class="comment_meta">

                                <div class="comment_author">Admin - <i>May 12, 2014</i></div>                   

                            </div>
                            <div class="comment_text">
                                <p>Lorem ipsum dolor sit amet, consectetur rius a auctor enim accumsan.</p>
                                <a href="#">Reply</a>
                            </div>
                        </div>
                    </div><!-- end section -->

                    <div class="comment_wrap chaild">
                        <div class="gravatar"><img src="../../images/blog/people_img.jpg" alt="" /></div>
                        <div class="comment_content">
                            <div class="comment_meta">

                                <div class="comment_author">Admin - <i>May 12, 2014</i></div>                   

                            </div>
                            <div class="comment_text">
                                <p>Lorem ipsum dolor sit amet, consectetur rius a auctor enim accumsan.</p>
                                <a href="#">Reply</a>
                            </div>
                        </div>
                    </div><!-- end section -->

                    <div class="comment_wrap chaild">
                        <div class="gravatar"><img src="images/blog/people_img.jpg" alt="" /></div>
                        <div class="comment_content">
                            <div class="comment_meta">

                                <div class="comment_author">Admin - <i>May 12, 2014</i></div>                   

                            </div>
                            <div class="comment_text">
                                <p>Lorem ipsum dolor sit amet, consectetur rius a auctor enim accumsan.</p>
                                <a href="#">Reply</a>
                            </div>
                        </div>
                    </div><!-- end section -->


                    <div class="comment_form">

                        <h4>Để lại bình luận của bạn</h4>

                        <form action="blog-post.html" method="post">
                            <input type="text" name="yourname" id="name" class="comment_input_bg" />
                            <label for="name">Tên*</label>

                            <input type="text" name="email" id="mail" class="comment_input_bg" />
                            <label for="mail">Mail*</label>

                            <textarea name="comment" class="comment_textarea_bg" rows="20" cols="7" ></textarea>
                            <div class="clearfix"></div> 
                            <input name="send" type="submit" value="Gửi" class="comment_submit"/>
                            <p></p>
                            <p class="comment_checkbox"><input type="checkbox" name="check" /> Thông báo qua email cho tôi</p>


                        </form>

                    </div><!-- end comment form -->

                    <div class="clearfix mar_top2"></div>  

                </div><!-- end content left side -->

                <!-- right sidebar starts -->
                <!--                <div class="right_sidebar">
                
                                    <div class="sidebar_widget">
                
                                        <div class="sidebar_title"><h4>Site <i>Categories</i></h4></div>
                                        <ul class="arrows_list1">		
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Economics</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Social Media</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Economics</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Online Gaming</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Entertainment</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Technology</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Make Money Online</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Photography</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i> Web Tutorials</a></li>
                                        </ul>
                
                                    </div> end section -->

                <!--                    <div class="clearfix margin_top4"></div>
                
                                    <div class="sidebar_widget">
                
                                        <div id="tabs">
                
                                            <ul class="tabs">  
                                                <li class="active"><a href="#tab1">Popular</a></li>
                                                <li><a href="#tab2">Recent</a></li>
                                                <li class="last"><a href="#tab3">Tags</a></li>
                                            </ul> /# end tab links  
                
                                            <div class="tab_container">	
                                                <div id="tab1" class="tab_content"> 
                
                                                    <ul class="recent_posts_list">
                                                        <li>
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Publishing packag esanse web page editos</a>
                                                            <i>July 13, 2014</i> 
                                                        </li>	
                                                        <li>
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Sublishing packag esanse web page editos</a>
                                                            <i>July 12, 2014</i> 
                                                        </li>	
                                                        <li class="last">
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Mublishing packag esanse web page editos</a>
                                                            <i>July 11, 2014</i> 
                                                        </li>
                                                    </ul>
                
                                                </div> end popular posts  
                
                                                <div id="tab2" class="tab_content">	 
                                                    <ul class="recent_posts_list">
                
                                                        <li>
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Various versions has evolved over the years</a>
                                                            <i>July 18, 2014</i> 
                                                        </li>
                                                        <li>
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Rarious versions has evolve over the years</a>
                                                            <i>July 17, 2014</i> 
                                                        </li>
                                                        <li class="last">
                                                            <span><a href="#"><img src="http://placehold.it/50x50" alt="" /></a></span>
                                                            <a href="#">Marious versions has evolven over the years</a>
                                                            <i>July 16, 2014</i> 
                                                        </li>
                                                    </ul>
                
                                                </div> end popular articles 	
                
                                                <div id="tab3" class="tab_content">	 
                                                    <ul class="tags">
                
                                                        <li><a href="#">2014</a></li>
                                                        <li><a href="#"><b>Amazing</b></a></li>
                                                        <li><a href="#">Animation</a></li>
                                                        <li><a href="#">Beautiful</a></li>
                                                        <li><a href="#"><b>Cartoon</b></a></li>
                                                        <li><a href="#">Comedy</a></li>
                                                        <li><a href="#"><b>Cool</b></a></li>
                                                        <li><a href="#">Dance</a></li>
                                                        <li><a href="#">Drive</a></li>
                                                        <li><a href="#"><b>Family</b></a></li>
                                                        <li><a href="#"><b>Fantasy</b></a></li>
                                                        <li><a href="#">News</a></li>
                                                        <li><a href="#"><b>Friends</b></a></li>
                                                        <li><a href="#">Funny</a></li>
                                                        <li><a href="#"><b>Games</b></a></li>
                                                        <li><a href="#">Love</a></li>
                                                        <li><a href="#"><b>Music</b></a></li>
                                                        <li><a href="#">Nature</a></li>
                                                        <li><a href="#"><b>Party</b></a></li>
                                                        <li><a href="#">Pictures</a></li>
                                                        <li><a href="#">Sports</a></li>
                                                        <li><a href="#"><b>Best</b></a></li>
                                                        <li><a href="#">Wedding</a></li>
                                                        <li><a href="#">Weight</a></li>
                                                        <li><a href="#"><b>Youtube</b></a></li>
                                                    </ul>	 
                                                </div>
                
                                            </div>
                
                                        </div>
                
                                    </div> end section 
                
                                    <div class="clearfix margin_top5"></div>
                
                                    <div class="clientsays_widget">
                
                                        <div class="sidebar_title"><h4>Happy <i>Client Say's</i></h4></div>
                
                                        <img src="http://placehold.it/50x50" alt="" />
                                        <strong>- Henry Brodie</strong><p>Lorem Ipsum passage, and going through the cites of the word here classical literature passage discovere there undou btable source looks reasonable the generated charac eristic words.</p>  
                
                                    </div> end section 
                
                                    <div class="clearfix margin_top4"></div>
                
                                    <div class="sidebar_widget">
                
                                        <div class="sidebar_title"><h4>Portfolio <i>Widget</i></h4></div>
                
                
                
                                    </div> end section 
                
                                    <div class="clearfix margin_top3"></div>
                
                                    <div class="sidebar_widget">
                
                                        <div class="sidebar_title"><h4>Site <i>Advertisements</i></h4></div>
                
                                        <ul class="adsbanner-list">  
                                            <li><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
                                            <li class="last"><a href="#"><img src="images/sample-ad-banner.jpg" alt="" /></a></li>
                                        </ul>
                
                                        <ul class="adsbanner-list">  
                                            <li><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
                                            <li class="last"><a href="#"><img src="../../images/sample-ad-banner.jpg" alt="" /></a></li>
                                        </ul>
                
                                    </div> end section 
                
                                    <div class="clearfix margin_top4"></div>
                
                                    <div class="sidebar_widget">
                
                                        <div class="sidebar_title"><h3>Site <i>Archives</i></h3></div>
                
                                        <ul class="arrows_list1">		
                                            <li><a href="#"><i class="fa fa-angle-right"></i> July 2014</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i> June 2014</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i> May 2014</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i> April 2014</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i> March 2014</a></li>
                                        </ul>
                
                                    </div> end section 
                
                                </div> end right sidebar -->


            </div><!-- end content area -->


            <div class="clearfix margin_top7"></div>



            <jsp:include page="../Layout/footer.jsp"></jsp:include>


        </div>


        <!-- ######### JS FILES ######### -->
        <!-- get jQuery from the google apis -->
        <script type="text/javascript" src="../../js/universal/jquery.js"></script>

        <!-- style switcher -->
        <script src="../../js/style-switcher/jquery-1.js"></script>
        <script src="../../js/style-switcher/styleselector.js"></script>

        <!-- animations -->
        <script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


        <!-- slide panel -->
        <script type="text/javascript" src="../../js/slidepanel/slidepanel.js"></script>

        <!-- mega menu -->
        <script src="../../js/mainmenu/bootstrap.min.js"></script> 
        <script src="../../js/mainmenu/customeUI.js"></script> 

        <!-- jquery jcarousel -->
        <script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

        <!-- tab widget -->
        <script type="text/javascript" src="../../js/tabs/tabwidget/tabwidget.js"></script>

        <!-- scroll up -->
        <script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

        <!-- tabs -->
        <script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});
        </script>
        <!-- jquery jcarousel -->
        <script type="text/javascript">
                                                (function ($) {
                                                    "use strict";

                                                    jQuery(document).ready(function () {
                                                        jQuery('#mycarouselthree').jcarousel();
                                                    });

                                                })(jQuery);
        </script>

        <!-- accordion -->
        <script type="text/javascript" src="../../js/accordion/custom.js"></script>

        <!-- sticky menu -->
        <script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
        <script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

        <!-- cubeportfolio -->
        <script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>

        <script type="text/javascript" src="../../js/cubeportfolio/main4.js"></script>
        <script type="text/javascript" src="../../js/cubeportfolio/main6.js"></script>

        <!-- carousel -->
        <script defer src="../../js/carousel/jquery.flexslider.js"></script>
        <script defer src="../../js/carousel/custom.js"></script>

        <!-- lightbox -->
        <script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="../../js/lightbox/custom.js"></script>



    </body>
</html>
