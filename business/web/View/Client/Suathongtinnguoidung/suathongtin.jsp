﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Hoxa - Multipurpose HTML5 Template</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />
    
    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->
    
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="../../js/slidepanel/slidepanel.css">
    
	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs2.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs3.css">

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="../../js/progressbar/ui.progress-bar.css">
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
	
    <!-- forms -->
    <link rel="stylesheet" href="../../js/form/sky-forms.css" type="text/css" media="all">
 
</head>

<body>


<div class="site_wrapper">


<!-- header -->
<jsp:include page="../Layout/header.jsp"></jsp:include>
<!-- end header -->

<div class="page_title2">
<div class="container">

    <div class="title"><h1>Cập nhật thông tin</h1></div>
    
    <div class="pagenation">&nbsp;<a href="../Trangchu/index.jsp">Trang chủ</a> <i>/</i> <a>Sửa thông tin người dùng</a> <i>/</i>việt anh</div>
</div>
</div><!-- end page title --> 

<div class="clearfix"></div>

<div class="container">

	<div class="content_fullwidth">
    
		
        <div class="reg_form">
       <section class="content">
        
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật thông tin</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                   <div class="form-group">
                  <label for="exampleInputEmail1">Tên tài khoản</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Điền họ tên">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Địa chỉ</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Điền địa chỉ">
                </div>
                  <div class="form-group">
                <label>Năm sinh</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
                  
                <div class="form-group">
                  <label for="exampleInputPassword1">Điện thoại</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Điền số điện thoại">
                </div>
                  
                 
                <div class="form-group">
                  <label for="exampleInputFile">Hình ảnh</label>
                  <input type="file" id="exampleInputFile">
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox"> Tôi đã đọc và đồng ý
                  </label>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
          </div>
        
    </section>		
		</div>
        
	</div>

</div><!-- end content area -->

<div class="clearfix margin_top7"></div>

<!-- footer -->
<jsp:include page="../Layout/footer.jsp"></jsp:include>
<!-- end footer -->


</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- slide panel -->
<script type="text/javascript" src="../../js/slidepanel/slidepanel.js"></script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script> 
<script src="../../js/mainmenu/customeUI.js"></script> 

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<script src="../../js/form/jquery.form.min.js"></script>
<script src="../../js/form/jquery.validate.min.js"></script>
<script src="../../js/form/jquery.modal.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

	$(function()
	{
		// Validation		
		$("#sky-form").validate(
		{					
			// Rules for form validation
			rules:
			{
				username:
				{
					required: true
				},
				email:
				{
					required: true,
					email: true
				},
				password:
				{
					required: true,
					minlength: 3,
					maxlength: 20
				},
				passwordConfirm:
				{
					required: true,
					minlength: 3,
					maxlength: 20,
					equalTo: '#password'
				},
				firstname:
				{
					required: true
				},
				lastname:
				{
					required: true
				},
				gender:
				{
					required: true
				},
				terms:
				{
					required: true
				}
			},
			
			// Messages for form validation
			messages:
			{
				login:
				{
					required: 'Please enter your login'
				},
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				},
				password:
				{
					required: 'Please enter your password'
				},
				passwordConfirm:
				{
					required: 'Please enter your password one more time',
					equalTo: 'Please enter the same password as above'
				},
				firstname:
				{
					required: 'Please select your first name'
				},
				lastname:
				{
					required: 'Please select your last name'
				},
				gender:
				{
					required: 'Please select your gender'
				},
				terms:
				{
					required: 'You must agree with Terms and Conditions'
				}
			},					
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
	});			

})(jQuery);
</script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main5.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main6.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>



</body>
</html>
