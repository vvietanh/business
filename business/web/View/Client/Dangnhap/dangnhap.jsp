﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>khoinghiep.com | Đăng Nhập</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../../images/favicon.ico">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../../css/reset.css" type="text/css" />
	<link rel="stylesheet" href="../../css/style.css" type="text/css" />
    
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../../css/responsive-leyouts.css" type="text/css" />
    
    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- mega menu -->
    <link href="../../js/mainmenu/stickytwo.css" rel="stylesheet">
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu.css" rel="stylesheet">
    
    <!-- slide panel -->
    <link rel="stylesheet" type="text/css" href="../../js/slidepanel/slidepanel.css">
    
	<!-- cubeportfolio -->
	<link rel="stylesheet" type="text/css" href="../../js/cubeportfolio/cubeportfolio.min.css">
    
	<!-- tabs -->
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs2.css">
    <link rel="stylesheet" type="text/css" href="../../js/tabs/assets/css/responsive-tabs3.css">

	<!-- carousel -->
    <link rel="stylesheet" href="../../js/carousel/flexslider.css" type="text/css" media="screen" />
 	<link rel="stylesheet" type="text/css" href="../../js/carousel/skin.css" />
    
    <!-- progressbar -->
  	<link rel="stylesheet" href="../../js/progressbar/ui.progress-bar.css">
    
    <!-- accordion -->
    <link rel="stylesheet" href="../../js/accordion/accordion.css" type="text/css" media="all">
    
    <!-- Lightbox -->
    <link rel="stylesheet" type="text/css" href="../../js/lightbox/jquery.fancybox.css" media="screen" />
	
    <!-- forms -->
    <link rel="stylesheet" href="../../js/form/sky-forms.css" type="text/css" media="all">
    
 
</head>

<body>


<div class="site_wrapper">

<jsp:include page="../Layout/header.jsp"></jsp:include>

<div class="clearfix"></div>

<div class="page_title2">
<div class="container">

    <div class="title"><h1>Đăng nhập</h1></div>
    
    <div class="pagenation">&nbsp;<a href="index.html">Home</a> <i>/</i> <a href="#">Pages</a> <i>/</i> Đăng nhập</div>
    
</div>
</div><!-- end page title -->  

<div class="clearfix"></div>

<div class="container">

	<div class="content_fullwidth">
    
		
      <div class="login_form">		
			<form id="sky-form" class="sky-form" action="${root}/business/TaiKhoanServlet1" method="POST">
				<header>Đăng nhập</header>
				<%if (request.getParameter("error")!= null) 
                            {   
                            %>
                            <p style="color: pink"> 
                                <%=request.getParameter("error")%>
                            </p>
                            <%
                                }
                            %>
				<fieldset>					
					<section>
						<div class="row">
							<label class="label col col-4">Email</label>
							<div class="col col-8">
								<label class="input">
									<i class="icon-append icon-user"></i>
                                                                        <input type="email" name="email" id="email">
								</label>
							</div>
						</div>
					</section>
					
					<section>
						<div class="row">
							<label class="label col col-4">Mật khẩu</label>
							<div class="col col-8">
								<label class="input">
									<i class="icon-append icon-lock"></i>
									<input type="password" name="matKhau" id="matKhau">
								</label>
								<div class="note"><a href="#sky-form2" class="modal-opener">Quên mật khẩu ?</a></div>
							</div>
						</div>
					</section>
					
					<section>
						<div class="row">
							<div class="col col-4"></div>
							<div class="col col-8">
								<label class="checkbox"><input type="checkbox" name="remember" checked><i></i> Giữ tài khoản đăng nhập! </label>
							</div>
						</div>
					</section>
				</fieldset>
				<footer>
					<div class="fright">
                    <a href="../Dangky/dangky.jsp" class="button button-secondary">Đăng kí</a>
                    <button type="submit" name="command" class="button" value="login">Đăng nhập</button>
                    </div>
					
				</footer>
			</form>			
		</div>
        
		<form action="demo-recovery.php" id="sky-form2" class="sky-form sky-form-modal">
			<header>Khôi phục mật khẩu!</header>
			
			<fieldset>					
				<section>
					<label class="label">E-mail</label>
					<label class="input">
						<i class="icon-append icon-user"></i>
						<input type="email" name="email" id="email">
					</label>
				</section>
			</fieldset>
			
			<footer>
				<button type="submit" name="submit" class="button">Đăng kí</button>
				<a href="#" class="button button-secondary modal-closer">Close</a>
			</footer>
				
			<div class="message">
				<i class="icon-ok"></i>
				<p>Your request successfully sent!<br><a href="#" class="modal-closer">Close window</a></p>
			</div>
		</form>

	</div>

</div><!-- end content area -->

<div class="clearfix margin_top7"></div>

<div class="footer1">
<div class="container">
	    
	<div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="200">
        <ul class="faddress">
            <li><img src="../../images/footer-logo.png" alt="" /></li>
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 2901 Marmora Road, Glassgow,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seattle, WA 98122-1090</li>
            <li><i class="fa fa-phone"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><i class="fa fa-print"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><a href="mailto:info@yourdomain.com"><i class="fa fa-envelope"></i> info@yourdomain.com</a></li>
            <li><img src="../../images/footer-wmap.png" alt="" /></li>
        </ul>
	</div><!-- end address -->
    
    <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="300">
    <div class="qlinks">
    
    	<h4 class="lmb">Useful Links</h4>
        
        <ul>
            <li><a href="#"><i class="fa fa-angle-right"></i> Home Page Variations</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Awsome Slidershows</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Features and Typography</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Different &amp; Unique Pages</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Single and Portfolios</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Recent Blogs or News</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Layered PSD Files</a></li>
        </ul>
        
    </div>
	</div><!-- end links -->
        
    <div class="one_fourth animate" data-anim-type="fadeInUp" data-anim-delay="400">
    <div class="siteinfo">
    
    	<h4 class="lmb">About Hoxa</h4>
        
        <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined an chunks as necessary, making this the first true generator on the Internet. Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites.</p>
        <br />
        <a href="#">Read more <i class="fa fa-long-arrow-right"></i></a>
        
	</div>
    </div><!-- end site info -->
    
    <div class="one_fourth last animate" data-anim-type="fadeInUp" data-anim-delay="500">
        
        <h4>Flickr Photos</h4>
        
        <div id="flickr_badge_wrapper">
            <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=h&amp;source=user&amp;user=93382411%40N07"></script>     
        </div>
        
    </div><!-- end flickr -->
    
    
</div>
</div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info four">
<div class="container">
    
    <div class="one_half">
    
        Copyright © 2014 hoxa.com. All rights reserved.  <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links three">
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="200"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="250"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="350"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="400"><a href="#"><i class="fa fa-skype"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="450"><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="500"><a href="#"><i class="fa fa-html5"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="550"><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="600"><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->





</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="../../js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="../../js/style-switcher/jquery-1.js"></script>
<script src="../../js/style-switcher/styleselector.js"></script>

<!-- animations -->
<script src="../../js/animations/js/animations.min.js" type="text/javascript"></script>


<!-- slide panel -->
<script type="text/javascript" src="../../js/slidepanel/slidepanel.js"></script>

<!-- mega menu -->
<script src="../../js/mainmenu/bootstrap.min.js"></script> 
<script src="../../js/mainmenu/customeUI.js"></script> 

<!-- jquery jcarousel -->
<script type="text/javascript" src="../../js/carousel/jquery.jcarousel.min.js"></script>

<!-- scroll up -->
<script src="../../js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- tabs -->
<script src="../../js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>

<!-- jquery jcarousel -->
<script type="text/javascript">
(function($) {
 "use strict";

	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
})(jQuery);
</script>

<!-- accordion -->
<script type="text/javascript" src="../../js/accordion/custom.js"></script>

<!-- sticky menu -->
<script type="text/javascript" src="../../js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../../js/mainmenu/modernizr.custom.75180.js"></script>

<script src="../../js/form/jquery.form.min.js"></script>
<script src="../../js/form/jquery.validate.min.js"></script>
<script src="../../js/form/jquery.modal.js"></script>

<script type="text/javascript">
(function($) {
 "use strict";

	$(function()
	{
		// Validation for login form
		$("#sky-form").validate(
		{					
			// Rules for form validation
			rules:
			{
				email:
				{
					required: true,
					email: true
				},
				password:
				{
					required: true,
					minlength: 3,
					maxlength: 20
				}
			},
								
			// Messages for form validation
			messages:
			{
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				},
				password:
				{
					required: 'Please enter your password'
				}
			},					
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
		
		
		// Validation for recovery form
		$("#sky-form2").validate(
		{					
			// Rules for form validation
			rules:
			{
				email:
				{
					required: true,
					email: true
				}
			},
								
			// Messages for form validation
			messages:
			{
				email:
				{
					required: 'Please enter your email address',
					email: 'Please enter a VALID email address'
				}
			},
								
			// Ajax form submition					
			submitHandler: function(form)
			{
				$(form).ajaxSubmit(
				{
					success: function()
					{
						$("#sky-form2").addClass('submited');
					}
				});
			},				
			
			// Do not change code below
			errorPlacement: function(error, element)
			{
				error.insertAfter(element.parent());
			}
		});
	});			

})(jQuery);
</script>

<!-- cubeportfolio -->
<script type="text/javascript" src="../../js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main5.js"></script>
<script type="text/javascript" src="../../js/cubeportfolio/main6.js"></script>

<!-- carousel -->
<script defer src="../../js/carousel/jquery.flexslider.js"></script>
<script defer src="../../js/carousel/custom.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="../../js/lightbox/jquery.fancybox.js"></script>
<script type="text/javascript" src="../../js/lightbox/custom.js"></script>



</body>
</html>
