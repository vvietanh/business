<%-- 
    Document   : footer
    Created on : Apr 19, 2017, 8:47:47 AM
    Author     : Kaka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
<div class="footer1">
<div class="container">
	    
	<div class="one_fourth1 animate" data-anim-type="fadeInUp" data-anim-delay="200">
        <ul class="faddress">
            <li><img src="../../images/care-512.png" width="100" height="100"  alt="" /></li>
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; Số 1 Pastuer, Quận 1,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TP. Hồ Chí Minh, WA 98122-1090</li>
            <li><i class="fa fa-phone"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><i class="fa fa-print"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><a href="mailto:info@yourdomain.com"><i class="fa fa-envelope"></i> info@yourdomain.com</a></li>
            <li><img src="../../images/footer-wmap.png" alt="" /></li>
        </ul>
	</div><!-- end address -->
    
</div>
</div><!-- end footer -->


<div class="clearfix"></div>

<div class="copyright_info four">
<div class="container">
    
    <div class="one_half">
    
        Copyright © 2017 startupvn.com.  <a href="#">Điều khoản sử dụng</a> | <a href="#">Chính sách bảo mật</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links three">
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="200"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="300"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="400"><a href="#"><i class="fa fa-skype"></i></a></li>
            <li class="animate" data-anim-type="zoomIn" data-anim-delay="550"><a href="#"><i class="fa fa-youtube"></i></a></li>
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->




    </body>
</html>
