<%-- 
    Document   : header
    Created on : Apr 19, 2017, 8:44:53 AM
    Author     : Kaka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
    </head>
    <body>
        <header id="header">

        <div id="trueHeader">

        <div class="wrapper">


            <div class="logoarea">
            <div class="container">

            <!-- Logo -->
            <div class="logo"><a href="index.jsp" id="logo"></a></div>

                <div class="right_links">

                <ul>
                    <li class="link"><a class="fancybox fancybox.ajax" href="login-frame.html"><i class="fa fa-envelope"></i> info@yourwebsite.com</a></li>
                    <li class="link"><a href="../Dangnhap/dangnhap.jsp"><i class="fa fa-edit"></i> Đăng Nhập</a></li>
                                <li class="link"><a class="fancybox fancybox.ajax" href="register-frame.html"><i class="fa fa-bell" aria-hidden="true"></i>Thông Báo</a></li>


                                <li class="social"><a href="#"><i class="fa fa-language" aria-hidden="true"></i></a></li>
                </ul>

            </div><!-- end right links -->


            </div>
            </div>

                <!-- Menu -->
                <div class="menu_main">

            <div class="container">

                <div class="navbar yamm navbar-default">

            <div class="container">
              <div class="navbar-header">
                <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  > <span>Menu</span>
                  <button type="button" > <i class="fa fa-bars"></i></button>
                </div>
              </div>

              <div id="navbar-collapse-1" class="navbar-collapse collapse">

                <ul class="nav navbar-nav">

                <li><a href="index.html" class="active">Trang Chủ</a></li>

                <li class="dropdown"> <a href="../Tintuc/danhsach.jsp">Bảng Tin</a></li>

                <li class="dropdown"> <a href="../Lienhe/lienhe.jsp">Liên Hệ</a></li>


                </ul>

                <div id="wrap">
                  <form action="index.html" autocomplete="on">
                  <input id="search" name="search" type="text" placeholder=""><input id="search_submit" value="search" type="submit">

                  </form>
                </div>  

              </div>
              </div>
             </div>

                </div>
            </div><!-- end menu -->

                </div>

                </div>

        </header>

        <div class="clearfix"></div>

    </body>
</html>
