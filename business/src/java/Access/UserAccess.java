/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.DoanhNghiepModel;
import Model.TaiKhoanModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vi_Ngo
 */
public class UserAccess {

    //kiểm tra email có trùng hay không ?
    public boolean checkEmail(String EmailUser) {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM taikhoan WHERE Email = '" + EmailUser + "'";
        PreparedStatement ps;
        try {
            ps = connection.prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                connection.close();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    // thêm tài khoản
    public boolean insert(TaiKhoanModel tk) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO taikhoan(HoTen,MatKhau,Email,DienThoai,DiaChi,TrangThai,AnhDaiDien) VALUE(?,?,?,?,?,?,?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
            ps.setString(1,tk.gethoTen());
            ps.setString(2,tk.getMatKhau());
            ps.setString(3,tk.getEmail());
            ps.setInt(4, tk.getDienThoai());
            ps.setString(5,tk.getDiaChi());
            ps.setInt(6, tk.getTrangThai());
            ps.setString(7, tk.getAnhDaiDien());
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
        
            
        
    }

    //dang nhap tai khoan
    public TaiKhoanModel login(String Email, String MatKhau) {
        Connection con = DBConnect.connectDB();
        String sql = "SELECT * FROM taikhoan WHERE Email='" + Email + "' AND MatKhau='" + MatKhau + "'";
        PreparedStatement ps;
        try {
            ps = (PreparedStatement) con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            TaiKhoanModel tk = new TaiKhoanModel();
            if (rs.next()) {
                tk.setEmail(rs.getString("Email"));
                tk.setMatKhau(rs.getString("MatKhau"));
                return tk;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public TaiKhoanModel getPosterbyID(int maTK) {
        Connection con = DBConnect.connectDB();
        String sql = "SELECT * FROM taikhoan WHERE MaTK="+maTK;
        PreparedStatement ps;
        try {
            ps = (PreparedStatement) con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            TaiKhoanModel tk = new TaiKhoanModel();
            if (rs.next()) {
                tk.setEmail(rs.getString("Email"));
                tk.setDiaChi(rs.getString("DiaChi"));
                tk.setDienThoai(rs.getInt("DienThoai"));
                tk.setHoTen(rs.getString("HoTen"));
                return tk;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ArrayList<TaiKhoanModel> getListTk() throws SQLException{
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM taikhoan";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<TaiKhoanModel>list = new ArrayList<>();
        while(rs.next()){
            TaiKhoanModel dn = new TaiKhoanModel();
            dn.setMaTK(rs.getInt("maTK"));
            dn.setHoTen(rs.getString("hoTen"));
            list.add(dn);
        }
        return list;        
    }

    public static void main(String[] args) throws SQLException {
        UserAccess useraccess = new UserAccess();
        
        System.out.println( useraccess.insert(new TaiKhoanModel("Thuyvi1", "123456", "Thuyvi1041@gmail.com", 121312312, "123vanhanh", 1,"abc")));
//        for (TaiKhoanModel tk : useraccess.getListTk()){
//            System.out.println(tk.getMaTK()+"----"+tk.getHoTen());
//        }

    }
}

