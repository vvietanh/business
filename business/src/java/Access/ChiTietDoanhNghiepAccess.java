/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.ChiTietDoanhNghiepModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author VIETANH
 */
public class ChiTietDoanhNghiepAccess {
     public ChiTietDoanhNghiepModel getLinhVucByIdLV(int maDN)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM chitiet_doanhnghiep WHERE MaDN ="+maDN;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ChiTietDoanhNghiepModel ctdn = new ChiTietDoanhNghiepModel();
        while(rs.next()){  
            ctdn.setMaLV(rs.getInt("MaLV"));
            ctdn.setMaDN(rs.getInt("MaDN"));
        }
        return ctdn;
    }
     
      public static void main(String[] args) throws SQLException {
        ChiTietDoanhNghiepAccess ctdn = new ChiTietDoanhNghiepAccess();

        System.out.println(ctdn.getLinhVucByIdLV(1));

    }
}
