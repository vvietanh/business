/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;

import Model.TinTucModel;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TinTucAccess {
    

    // lấy danh sách tin tuc
   public ArrayList<TinTucModel> getListTinTuc() throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM tintuc";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<TinTucModel> list = new ArrayList<>();
        while (rs.next()) {
            TinTucModel tintuc = new TinTucModel();
            tintuc.setMaTin(rs.getInt("MaTin"));
            tintuc.setTieuDe(rs.getString("TieuDe"));
            tintuc.setNoiDung(rs.getString("NoiDung"));
            tintuc.setMoTa(rs.getString("MoTa"));
            tintuc.setHinhAnh(rs.getString("HinhAnh"));
            tintuc.setNgayDang(rs.getDate("NgayDang"));
            tintuc.setNoiDung(rs.getString("NguoiDang"));
            list.add(tintuc);
        }
        return list;
    }
   // chi tiết tin tức 
   public TinTucModel getTinTuc(int MaTin) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM tintuc where MaTin = ' "+MaTin+" ' ";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        TinTucModel tintuc = new TinTucModel();
        while (rs.next()) {
           
            tintuc.setMaTin(rs.getInt("MaTin"));
            tintuc.setTieuDe(rs.getString("TieuDe"));
            tintuc.setNoiDung(rs.getString("NoiDung"));
            tintuc.setMoTa(rs.getString("MoTa"));
            tintuc.setHinhAnh(rs.getString("HinhAnh"));
            tintuc.setNgayDang(rs.getDate("NgayDang"));
            tintuc.setNoiDung(rs.getString("NguoiDang"));
            
        }
        return tintuc;
    }

    // thêm tin tức 
    public boolean insert(TinTucModel tt) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO tintuc(TieuDe, NoiDung, MoTa, HinhAnh, NguoiDang) VALUE(?,?,?,?,?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
             
             ps.setString(1, tt.getTieuDe());
             ps.setString(2,tt.getNoiDung());
             ps.setString(3,tt.getMoTa());
             ps.setString(4,tt.getHinhAnh());
             ps.setDate(5, (Date) tt.getNgayDang());
             ps.setInt(5,tt.getNguoiDang());
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
    }

    // xóa tin tuc
    public boolean delete(int ma) throws SQLException {
        try {
            Connection connection = DBConnect.connectDB();
            String sql = "DELETE FROM tintuc WHERE MaTin = ?";
            PreparedStatement ps = connection.prepareCall(sql);
            ps.setInt(1, ma);
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean update(int ma, TinTucModel tt) throws SQLException {
            Connection connection = DBConnect.connectDB();
             String sql = "UPDATE tintuc SET TieuDe = ?,NoiDung=?, MoTa=?, HinhAnh=?, NgayDang=?, NguoiDang=?";
        try {
             
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, tt.getTieuDe());
             ps.setString(2,tt.getNoiDung());
             ps.setString(3,tt.getMoTa());
             ps.setString(4,tt.getHinhAnh());
             ps.setDate(5, (Date) tt.getNgayDang());
             ps.setInt(6,tt.getNguoiDang());
             
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
           
             return false;
        }
    }
    
        
 

    public static void main(String[] args) throws SQLException {
       
//     GetCurrentDateTimeAccess date= new GetCurrentDateTimeAccess();
//  
//     // Hien thi date va time boi su dung toString()
//     String str = String.format("2017-02-02", date );
//
//     System.out.printf(str);
            
        TinTucAccess tintuc = new TinTucAccess();
      
//        System.out.println(tintuc.insert(new TinTucModel("HomNay", "Noidung la nhu the nay", "mo ta ngan gon thoi", "cb.png","2017-02-02",1)));
    }

    
}
