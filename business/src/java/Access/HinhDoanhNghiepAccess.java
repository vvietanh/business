/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.DoanhNghiepModel;
import Model.HinhDoanhNghiepModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author VIETANH
 */
public class HinhDoanhNghiepAccess {
    public HinhDoanhNghiepModel getHinhByMaDN(int maDN)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT DuongDan FROM hinhdoanhnghiep WHERE MaDN ="+maDN;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        HinhDoanhNghiepModel bd = new HinhDoanhNghiepModel();
        while(rs.next()){  
            bd.setDuongDan(rs.getString("DuongDan"));          
        }
        return bd;
    }
    public ArrayList<HinhDoanhNghiepModel> getListHinhDN(int maDN) throws SQLException{
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM hinhdoanhnghiep WHERE MaDN="+maDN;
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<HinhDoanhNghiepModel>list = new ArrayList<>();
        while(rs.next()){
            HinhDoanhNghiepModel dn = new HinhDoanhNghiepModel();
            dn.setDuongDan(rs.getString("DuongDan"));
            list.add(dn);
        }
        return list;        
    }
    public static void main (String[] args) throws SQLException {
        HinhDoanhNghiepAccess hbd = new HinhDoanhNghiepAccess();
//        dnct.delete(2);
        HinhDoanhNghiepModel md = hbd.getHinhByMaDN(2);
        System.out.println(md.getDuongDan());
        
    }
}
