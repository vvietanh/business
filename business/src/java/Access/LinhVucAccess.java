/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.LinhVucModel;
import Model.ChiTietDoanhNghiepModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author VIETANH
 */
public class LinhVucAccess {

    // lấy danh sách khu vực
    public ArrayList<LinhVucModel> getListLinhVuc() throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM linhvuc";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<LinhVucModel> list = new ArrayList<>();
        while (rs.next()) {
            LinhVucModel linhvuc = new LinhVucModel();
            linhvuc.setMaLV(rs.getInt("MaLV"));
            linhvuc.setTenLV(rs.getString("TenLV"));
            list.add(linhvuc);
        }
        return list;
    }
    
    public LinhVucModel getLinhVucByIdLV(int maLV)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM linhvuc WHERE MaLV ="+maLV;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        LinhVucModel linhvuc = new LinhVucModel();
        while(rs.next()){  
            linhvuc.setMaLV(rs.getInt("MaLV"));
            linhvuc.setTenLV(rs.getString("TenLV"));
        }
        return linhvuc;
    }
    

    // thêm danh khu vực
    public boolean insert(LinhVucModel lv) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO linhvuc(TenLV) VALUE(?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, lv.getTenLV());
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
    }

    // xóa khu vực
    public boolean delete(int ma) throws SQLException {
        try {
            Connection connection = DBConnect.connectDB();
            String sql = "DELETE FROM linhvuc WHERE MaLV = ?";
            PreparedStatement ps = connection.prepareCall(sql);
            ps.setInt(1, ma);
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean update(int ma, LinhVucModel lv) throws SQLException {
        try {
             Connection connection = DBConnect.connectDB();
             String sql = "UPDATE linhvuc SET TenLV = ? WHERE MaLV = ?";
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, lv.getTenLV());
             ps.setInt(2, ma);
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;
        }
    }

    public static void main(String[] args) throws SQLException {
        LinhVucAccess linhvuc = new LinhVucAccess();
//          for(LinhVucModel ds : linhvuc.getListLinhVuc()){
//              System.out.println(ds.getMaLV()+ " - " + ds.getTenLV()); 
//          }
//          
        //System.out.println(linhvuc.insert(new LinhVucModel("Đà Nẵng")));
   
       System.out.println(linhvuc.update(9, new LinhVucModel("abc")));
       // System.out.println(linhvuc.delete(1));
    }
}
