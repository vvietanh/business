/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Model.HinhBaiDangModel;
import Connect.DBConnect;
import Model.BaiDangModel;
import Model.HinhDoanhNghiepModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Vi_Ngo
 */
public class BaiDangAccess {
    public ArrayList<BaiDangModel> getListBD() throws SQLException{
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM baidang";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<BaiDangModel>list = new ArrayList<>();
        while(rs.next()){
            BaiDangModel bd = new BaiDangModel();
            bd.setMaBD(rs.getInt("maBD"));
            bd.setTieuDe(rs.getString("tieuDe"));
            bd.setNoiDung(rs.getString("noiDung"));
            bd.setMoTa(rs.getString("moTa"));
            bd.setNgayDang(rs.getDate("ngayDang"));
            bd.setTrangThaiXem(rs.getInt("trangThaiXem"));
            bd.setTrangThaiDuyet(rs.getInt("trangThaiDuyet"));
            bd.setNguoiDang(rs.getInt("nguoiDang"));
            bd.setNguoiDuyet(rs.getInt("nguoiDuyet"));
            list.add(bd);
        }
        return list;        
    }
    
    public BaiDangModel getBDByIdBD(int maBD)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM baidang WHERE MaBD ="+maBD;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        BaiDangModel bd = new BaiDangModel();
        while(rs.next()){  
            bd.setMaBD(rs.getInt("maBD"));
            bd.setTieuDe(rs.getString("tieuDe"));
            bd.setNoiDung(rs.getString("noiDung"));
            bd.setMoTa(rs.getString("moTa"));
            bd.setNgayDang(rs.getDate("ngayDang"));
            bd.setTrangThaiXem(rs.getInt("trangThaiXem"));
            bd.setTrangThaiDuyet(rs.getInt("trangThaiDuyet"));
            bd.setNguoiDang(rs.getInt("nguoiDang"));
            bd.setNguoiDuyet(rs.getInt("nguoiDuyet"));      
        }
        return bd;
    }
    public HinhBaiDangModel getHinhByMaBD(int maBD)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM hinhbaidang WHERE MaBD ="+maBD;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        HinhBaiDangModel hinhbd = new HinhBaiDangModel();
        while(rs.next()){  
            hinhbd.setDuongDan(rs.getString("DuongDan"));
            hinhbd.setMaBD(rs.getInt("MaBD"));
            hinhbd.setMaHinh(rs.getInt("MaHinh"));
        }
        return hinhbd;
    }
    public static void main(String[] args) throws SQLException {
        BaiDangAccess h = new BaiDangAccess();
        BaiDangModel hbd = h.getBDByIdBD(1);
        System.out.println(hbd.getNgayDang()+"------"+hbd.getMoTa());
        
    }
    
}
