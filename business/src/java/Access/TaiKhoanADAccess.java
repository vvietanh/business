/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.TaiKhoanADModel;
import Model.TaiKhoanADModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class TaiKhoanADAccess {

    // lấy danh sách tin tuc
    public ArrayList<TaiKhoanADModel> getListTaiKhoanAD() throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM taikhoan";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<TaiKhoanADModel> list = new ArrayList<>();
        while (rs.next()) {
            TaiKhoanADModel taikhoanad = new TaiKhoanADModel();
            taikhoanad.setMaTK(rs.getInt("MaTK"));
            taikhoanad.setHoTen(rs.getString("HoTen"));
            taikhoanad.setEmail(rs.getString("Email"));
            taikhoanad.setDienThoai(rs.getInt("DienThoai"));
            taikhoanad.setDiaChi(rs.getString("DiaChi"));
            taikhoanad.setTrangThai(rs.getInt("TrangThai"));
             taikhoanad.setAnhDaiDien(rs.getString("AnhDaiDien"));
            
            list.add(taikhoanad);
        }
        return list;
    }

    // thêm danh tai khoản 
    public boolean insert(TaiKhoanADModel tk) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO taikhoan(HoTen) VALUE(?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, tk.getHoTen());
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
    }

    // xóa tai khoan
    public boolean delete(int ma) throws SQLException {
        try {
            Connection connection = DBConnect.connectDB();
            String sql = "DELETE FROM taikhoan WHERE MaTK = ?";
            PreparedStatement ps = connection.prepareCall(sql);
            ps.setInt(1, ma);
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean update(int ma, TaiKhoanADModel tt) throws SQLException {
        try {
             Connection connection = DBConnect.connectDB();
             String sql = "UPDATE taikhoan SET HoTen = ? WHERE MaTK = ?";
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, tt.getHoTen());
             ps.setInt(2, ma);
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;
        }
    }

    public static void main(String[] args) throws SQLException {
        TaiKhoanADAccess taikhoanad = new TaiKhoanADAccess();
//          for(TaiKhoanADModel ds : khuvuc.getListTaiKhoanAD()){
//              Systm.out.println(ds.getMaKV() + " - " + ds.getTenKV()); 
//          }
          
       //   System.out.println(khuvuc.insert(new TaiKhoanADModel("Đà Nẵng   ")));
   
      // taikhoanad.insert(new TaiKhoanADModel("Tin Hot Trong Ngay"));
    }

   
}
