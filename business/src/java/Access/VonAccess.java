/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.VonModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author VIETANH
 */
public class VonAccess {

    // lấy danh sách khu vực
    public ArrayList<VonModel> getListVon() throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM von";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<VonModel> list = new ArrayList<>();
        while (rs.next()) {
            VonModel von = new VonModel();
            von.setMaVon(rs.getInt("MaVon"));
            von.setMucVon(rs.getString("MucVon"));
            von.setDonVi(rs.getString("DonVi"));
            list.add(von);
        }
        return list;
    }

    // thêm danh khu vực
    public boolean insert(VonModel von) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO von(MucVon, DonVi) VALUE(?,?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, von.getMucVon());
             ps.setString(2, von.getDonVi());
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
    }

    // xóa khu vực
    public boolean delete(int ma) throws SQLException {
        try {
            Connection connection = DBConnect.connectDB();
            String sql = "DELETE FROM von WHERE MaVon = ?";
            PreparedStatement ps = connection.prepareCall(sql);
            ps.setInt(1, ma);
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean update(int ma, VonModel von) throws SQLException {
        try {
             Connection connection = DBConnect.connectDB();
             String sql = "UPDATE von SET MucVon = ?, DonVi = ? WHERE MaVon = ?";
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, von.getMucVon());
             ps.setString(2, von.getDonVi());
             ps.setInt(3, ma);
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;
        }
    }

    public static void main(String[] args) throws SQLException {
        VonAccess von = new VonAccess();
         
          
//        System.out.println(von.insert(new VonModel(4000, "VNĐ")));
//   
//        System.out.println(von.update(3, new VonModel(5000, "VND")));
//        
//         for(VonModel ds : von.getListKhuVuc()){
//              System.out.println(ds.getMaVon() + " - " + ds.getMucVon() + " - " + ds.getDonVi()); 
//          }
    }
}
