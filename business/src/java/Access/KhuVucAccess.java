/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.KhuVucModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author VIETANH
 */
public class KhuVucAccess {

    // lấy danh sách khu vực
    public ArrayList<KhuVucModel> getListKhuVuc() throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM khuvuc";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<KhuVucModel> list = new ArrayList<>();
        while (rs.next()) {
            KhuVucModel khuvuc = new KhuVucModel();
            khuvuc.setMaKV(rs.getInt("MaKV"));
            khuvuc.setTenKV(rs.getString("TenKV"));
            list.add(khuvuc);
        }
        return list;
    }
    
     public KhuVucModel getKhuVucByIdKV(int maKV)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM khuvuc WHERE MaKV ="+maKV;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        KhuVucModel khuvuc = new KhuVucModel();
        while(rs.next()){  
            khuvuc.setMaKV(rs.getInt("MaKV"));
            khuvuc.setTenKV(rs.getString("TenKV"));
        }
        return khuvuc;
    }

    // thêm danh khu vực
    public boolean insert(KhuVucModel kv) throws SQLException {
        Connection connection = DBConnect.connectDB();
        String sql = "INSERT INTO khuvuc(TenKV) VALUE(?)";
        try {
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, kv.getTenKV());
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;  
        }
    }

    // xóa khu vực
    public boolean delete(int ma) throws SQLException {
        try {
            Connection connection = DBConnect.connectDB();
            String sql = "DELETE FROM khuvuc WHERE MaKV = ?";
            PreparedStatement ps = connection.prepareCall(sql);
            ps.setInt(1, ma);
            return ps.executeUpdate() == 1;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean update(int ma, KhuVucModel kv) throws SQLException {
        try {
             Connection connection = DBConnect.connectDB();
             String sql = "UPDATE khuvuc SET TenKV = ? WHERE MaKV = ?";
             PreparedStatement ps = connection.prepareCall(sql);
             ps.setString(1, kv.getTenKV());
             ps.setInt(2, ma);
             return ps.executeUpdate() == 1;
        } catch (Exception e) {
             return false;
        }
    }

    public static void main(String[] args) throws SQLException {
        KhuVucAccess khuvuc = new KhuVucAccess();
//          for(KhuVucModel ds : khuvuc.getListKhuVuc()){
//              System.out.println(ds.getMaKV() + " - " + ds.getTenKV()); 
//          }
          
       //   System.out.println(khuvuc.insert(new KhuVucModel("Đà Nẵng   ")));
   
        System.out.println(khuvuc.update(11, new KhuVucModel("abc")));
    }
}
