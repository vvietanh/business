/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Access;

import Connect.DBConnect;
import Model.DoanhNghiepModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jdt.internal.compiler.batch.Main;
import org.springframework.dao.support.DaoSupport;

/**
 *
 * @author Vi_Ngo
 */
public class DoanhNghiepAccess {
    public ArrayList<DoanhNghiepModel> getListDN() throws SQLException{
        Connection connection = DBConnect.connectDB();
        String sql = "SELECT * FROM doanhnghiep";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<DoanhNghiepModel>list = new ArrayList<>();
        while(rs.next()){
            DoanhNghiepModel dn = new DoanhNghiepModel();
            dn.setMaDN(rs.getInt("maDN"));
            dn.setDiaChi(rs.getString("diaChi"));
            dn.setDienThoai(rs.getInt("dienThoai"));
            dn.setTenDN(rs.getString("tenDN"));
            dn.setVon(rs.getInt("maVon"));
            dn.setMaBD(rs.getInt("maBD"));
            dn.setMaKV(rs.getInt("maKV"));
            dn.setMaThue(rs.getInt("maThue"));
            list.add(dn);
        }
        return list;        
    }
    public DoanhNghiepModel getDNbyidDN(int maDN)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM doanhnghiep WHERE MaDN ="+maDN;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        DoanhNghiepModel dn = new DoanhNghiepModel();
        while(rs.next()){
            dn.setMaDN(rs.getInt("maDN"));
            dn.setDiaChi(rs.getString("diaChi"));
            dn.setDienThoai(rs.getInt("dienThoai"));
            dn.setTenDN(rs.getString("tenDN"));
            dn.setVon(rs.getInt("maVon"));
            dn.setMaBD(rs.getInt("maBD"));
            dn.setMaKV(rs.getInt("maKV"));
            dn.setMaThue(rs.getInt("maThue"));       
        }
        return dn;
    }
    public DoanhNghiepModel getDNbyidBD(int maBD)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "SELECT * FROM doanhnghiep WHERE MaBD ="+maBD;
        PreparedStatement ps = cn.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        DoanhNghiepModel dn = new DoanhNghiepModel();
        while(rs.next()){
            dn.setMaDN(rs.getInt("maDN"));
            dn.setDiaChi(rs.getString("diaChi"));
            dn.setDienThoai(rs.getInt("dienThoai"));
            dn.setTenDN(rs.getString("tenDN"));
            dn.setVon(rs.getInt("maVon"));
            dn.setMaBD(rs.getInt("maBD"));
            dn.setMaKV(rs.getInt("maKV"));
            dn.setMaThue(rs.getInt("maThue"));       
        }
        return dn;
    }
    public boolean delete(int maDN) throws SQLException{
        try {
            Connection cn = DBConnect.connectDB();
            String sql = "DELETE FROM doanhnghiep WHERE MaDN="+maDN;
            PreparedStatement ps = cn.prepareCall(sql);
            int temp = ps.executeUpdate();
            return temp==1;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean update (DoanhNghiepModel dn)throws SQLException{
        Connection cn = DBConnect.connectDB();
        String sql = "UPDATE doanhnghiep SET TenDN =?,DiaChi = ?,MaThue = ?,DienThoai = ?,MaVon = ? WHERE MaDN = ? ";
        try {
            PreparedStatement ps = cn.prepareCall(sql);
            ps.setString(1, dn.getTenDN());
            ps.setString(2, dn.getDiaChi());
            ps.setInt(3, dn.getMaThue());
            ps.setInt(4, dn.getDienThoai());
            ps.setInt(5,dn.getVon());
            ps.setInt(6, dn.getMaDN());
            return ps.executeUpdate() == 1;
        } catch (Exception ex) {
            Logger.getLogger(DoanhNghiepAccess.class.getName()).log(Level.SEVERE,null,ex);
        }
        return false;
    }
    public static void main (String[] args) throws SQLException {
        DoanhNghiepAccess dnct = new DoanhNghiepAccess();
//        dnct.delete(2);
//        System.out.println(dnct.update(new DoanhNghiepModel(10, "Kiem Lien 1", 413441," 1213sgsdg",414141, 12, 1, 1)));
        
    }
}
