/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Access.TaiKhoanADAccess;
import Model.TaiKhoanADModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author asus
 */
public class TaiKhoanADServlet extends HttpServlet {

    TaiKhoanADAccess taikhoanadAccess = new TaiKhoanADAccess();
    TaiKhoanADModel taikhoanadModel = new TaiKhoanADModel();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8"); 
            
            String command = request.getParameter("command");
            String hoten = request.getParameter("txtHoTen");
            String matk = request.getParameter("MaTK");
            
            String url = "";
            try{
                switch(command){
                    case "update":
                        taikhoanadAccess.update(Integer.parseInt(matk), new TaiKhoanADModel(hoten));
                        url = "/business/View/Admin/Quanlytaikhoan/danhsach.jsp";
                        break;
                    case "delete":
                        taikhoanadAccess.delete(Integer.parseInt(matk));
                        url = "/business/View/Admin/Quanlytaikhoan/danhsach.jsp";
                        break;
                }   
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            
            String command = request.getParameter("command");
            String hoten = request.getParameter("txtHoTen");
            
            String url = "", error = "";
            if(hoten.equals("")){
                error = "vui lòng nhập họ tên tài khoản !";
                request.setAttribute("error", error);
            }
            try{
                if(error.length() == 0){
                    switch(command){
                        case "insert":
                            taikhoanadAccess.insert(new TaiKhoanADModel(hoten));
                            url = "/business/View/Admin/Quanlytaikhoan/danhsach.jsp";
                            break;
                    }
                }
                else{
                    url = "/business/View/Admin/Quanlytaikhoan/them.jsp";
                }
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);    
    }
    
    
}
