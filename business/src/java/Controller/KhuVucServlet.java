/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import Access.KhuVucAccess;
import Model.KhuVucModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author VIETANH
 */
public class KhuVucServlet extends HttpServlet {

    KhuVucAccess khuvucAccess = new KhuVucAccess();
    KhuVucModel khuvucModel = new KhuVucModel();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8"); 
            
            String command = request.getParameter("command");
            String tenkv = request.getParameter("txtKhuVuc");
            String makv = request.getParameter("MaKV");
            
            String url = "";
            try{
                switch(command){
                    
                    case "delete":
                        khuvucAccess.delete(Integer.parseInt(makv));
                        url = "/business/View/Admin/Quanlykhuvuc/danhsach.jsp";
                        break;
                }   
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            
            String command = request.getParameter("command");
            String tenkv = request.getParameter("txtKhuVuc");
            String makv = request.getParameter("MaKV");
            
            String url = "", error = "";
            if(tenkv.equals("")){
                error = "vui lòng nhập tên khu vực !";
                request.setAttribute("error", error);
            }
            try{
                if(error.length() == 0){
                    switch(command){
                        case "insert":
                            khuvucAccess.insert(new KhuVucModel(tenkv));
                            url = "/business/View/Admin/Quanlykhuvuc/danhsach.jsp";
                            break;
                            
                        case "update":
                        khuvucAccess.update(Integer.parseInt(makv), new KhuVucModel(tenkv));
                        url = "/business/View/Admin/Quanlykhuvuc/danhsach.jsp";
                        break;
                    }
                }
                else{
                    url = "/business/View/Admin/Quanlykhuvuc/them.jsp";
                }
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);    
    }
    
    
}
