/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Access.UserAccess;
import Model.TaiKhoanModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vi_Ngo
 */
public class TaiKhoanServlet1 extends HttpServlet {

    UserAccess userAccess = new UserAccess();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TaiKhoanServlet1</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TaiKhoanServlet1 at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String command = request.getParameter("command");
            
            String url = "";
            TaiKhoanModel tk = new TaiKhoanModel();
            HttpSession session = request.getSession();
            
            switch (command) {
                case "insert":
                    String Email = request.getParameter("email");
                    String HoTen = request.getParameter("hoTen");
                    String DiaChi = request.getParameter("diaChi");
                    int DienThoai = Integer.parseInt(request.getParameter("dienThoai"));
                    String MatKhau = request.getParameter("matKhau");
                    int TrangThai = 1;
                    String AnhDaiDien = "abc";
                    userAccess.insert(new TaiKhoanModel(HoTen, DiaChi, Email, DienThoai, DiaChi,TrangThai,AnhDaiDien));
                    url = "/business/View/Client/Dangnhap/dangnhap.jsp";
                    break;
                case "login":
                    tk = userAccess.login(request.getParameter("email"), request.getParameter("matKhau"));
                    if (tk != null) {
                        session.setAttribute("hoTen", tk);
                        url = "/business/View/Client/Trangchu/index.jsp";
                    } else {
                        request.setAttribute("error", "Tài khoản hoặc mật khẩu không đúng");
                        url = "/business/View/Client/Dangnhap/dangnhap.jsp";
                    }
                    break;
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);
        } catch (SQLException ex) {
            Logger.getLogger(TaiKhoanServlet1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
