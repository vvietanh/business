/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Access.DoanhNghiepAccess;
import Model.DoanhNghiepModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Vi_Ngo
 */
@WebServlet(name = "DoanhNghiepServlet", urlPatterns = {"/DoanhNghiepServlet"})
public class DoanhNghiepServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DoanhNghiepServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DoanhNghiepServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    DoanhNghiepAccess dn = new DoanhNghiepAccess();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String command = request.getParameter("command");
        String MaDN = request.getParameter("MaDN");
        
        String url = "";
        try {
            switch (command) {
                case "delete":
                    dn.delete(Integer.parseInt(MaDN));
                    url = "/business/View/Admin/quanlydoanhnghiep/danhsachdoanhnghiep.jsp";
                    break;
            }
        } catch (Exception e) {
            url = "/business/View/Admin/quanlydoanhnghiep/danhsachdoanhnghiep.jsp";
        }
        response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String command = request.getParameter("command");
        String MaDN = request.getParameter("MaDN");
        String TenDN = request.getParameter("TenDN");
        String MaThue = request.getParameter("MaThue");
        String DiaChi = request.getParameter("DiaChi");
        String DienThoai = request.getParameter("DienThoai");
        String MaVon    = request.getParameter("MaVon");
        String MaBD = request.getParameter("MaBD");
        String MaKV = request.getParameter("MaKV");
        String url = "";
        try {
            switch (command) {
                case "update":
                    dn.update(new DoanhNghiepModel(Integer.parseInt(MaDN),TenDN,Integer.parseInt(MaThue) , DiaChi, Integer.parseInt(DienThoai), 
                            Integer.parseInt(MaVon),Integer.parseInt(MaBD), Integer.parseInt(MaKV)));
                    url = "/business/View/Admin/Quanlydoanhnghiep/danhsachdoanhnghiep.jsp";
                    break;
            }
        } catch (Exception e) {
            url = "/business/View/Admin/Quanlydoanhnghiep/danhsachdoanhnghiep.jsp";
        }
        response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
