
package Controller;
import Access.TinTucAccess;
import Model.TinTucModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author asus
 */
public class TinTucServlet extends HttpServlet {

    TinTucAccess tintucAccess = new TinTucAccess();
    TinTucModel tintucModel = new TinTucModel();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8"); 
            
            String command = request.getParameter("command");
            String tieude = request.getParameter("txtTieuDe");
            String noidung =request.getParameter("txtNoiDung");
            String mota =request.getParameter("txtMoTa");
             String hinhanh =request.getParameter("txtHinhAnh");
             String ngaydang =request.getParameter("txtNgayDang");
              String nguoidang =request.getParameter("txtNguoiDang");
            String matin = request.getParameter("MaTin");
            
            String url = "";
            try{
                switch(command){
                    case "update":
                        tintucAccess.update(Integer.parseInt(matin), new TinTucModel(tieude));
                        url = "/business/View/Admin/Quanlytintuc/danhsach.jsp";
                        break;
                    case "delete":
                        tintucAccess.delete(Integer.parseInt(matin));
                        url = "/business/View/Admin/Quanlytintuc/danhsach.jsp";
                        break;
                }   
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            
            String command = request.getParameter("command");
            String tieude = request.getParameter("txtTinTuc");
            
            String url = "", error = "";
            if(tieude.equals("")){
                error = "vui lòng nhập tiêu đề tin tức !";
                request.setAttribute("error", error);
            }
            try{
                if(error.length() == 0){
                    switch(command){
                        case "insert":
                            tintucAccess.insert(new TinTucModel(tieude));
                            url = "/business/View/Admin/Quanlytintuc/danhsach.jsp";
                            break;
                    }
                }
                else{
                    url = "/business/View/Admin/Quanlytintuc/them.jsp";
                }
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);    
    }
    
    
}
