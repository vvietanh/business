/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Access.LinhVucAccess;
import Model.LinhVucModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author VIETANH
 */
@WebServlet(name = "LinhVucServlet", urlPatterns = {"/LinhVucServlet"})
public class LinhVucServlet extends HttpServlet {
    LinhVucAccess linhvucAccess = new LinhVucAccess();
    LinhVucModel linhvucModel = new LinhVucModel();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LinhVucServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LinhVucServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            request.setCharacterEncoding("utf-8");
            
            String command = request.getParameter("command");
            String tenlv = request.getParameter("txtLinhVuc");
            String malv = request.getParameter("MaLV");
            
            String url = "";
            try{
                switch(command){
                    
                    case "delete":
                        linhvucAccess.delete(Integer.parseInt(malv));
                        url = "/business/View/Admin/Quanlylinhvuc/danhsach.jsp";
                        break;
                }   
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            request.setCharacterEncoding("utf-8");
            
            String command = request.getParameter("command");
            String tenlv = request.getParameter("txtLinhVuc");
            String malv = request.getParameter("MaLV");
            
            String url = ""; 
//                    error = "";
//            if(tenlv.equals("")){
//                error = "vui lòng nhập tên lĩnh vực !";
//                request.setAttribute("error", error);
//            }
            try{
//                if(error.length() == 0){
                    switch(command){
                        case "insert":
                            linhvucAccess.insert(new LinhVucModel(tenlv));
                            url = "/business/View/Admin/Quanlylinhvuc/danhsach.jsp";
                            break;
                            
                        case "update":
                        linhvucAccess.update(Integer.parseInt(malv), new LinhVucModel(tenlv));
                        url = "/business/View/Admin/Quanlylinhvuc/danhsach.jsp";
                        break;
                    }
//                }
//                else{
//                    url = "/business/View/Admin/Quanlylinhvuc/them.jsp";
//                }
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);    
    }

    
}
