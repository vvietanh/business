/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Access.UserAccess;
import Model.TaiKhoanModel;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Vi_Ngo
 */
@WebServlet(name = "TaiKhoanServlet", urlPatterns = {"/TaiKhoanServlet"})
public class TaiKhoanServlet extends HttpServlet {
    UserAccess userAccess = new UserAccess();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TaiKhoanServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet TaiKhoanServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String command = request.getParameter("command");
        String url = "";
        TaiKhoanModel tk = new TaiKhoanModel();
        HttpSession session = request.getSession();
        switch (command){  
            case "insert":
                tk.setEmail(request.getParameter("email"));
                tk.sethoTen(request.getParameter("hoTen"));
                tk.setDiaChi(request.getParameter("diaChi"));
                tk.setDienThoai(Integer.parseInt(request.getParameter("dienThoai")));
                tk.setMatKhau(request.getParameter("matKhau"));
//                usersDAO.insertUser(users);
                session.setAttribute("email", tk);
                url = "/View/Client/TrangChu/index.jsp";
                break;
            case "login":
                tk = userAccess.login(request.getParameter("Email"), request.getParameter("matKhau"));
                if (tk != null){
                    session.setAttribute("email", tk);
                    url = "Client/TrangChu/index.jsp";
                }else{
                    request.setAttribute("error", "Tài khoản hoặc mật khẩu không đúng");
                    url = "/login_register.jsp";
                }
                break;
        }
        RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
        rd.forward(request, response);
    }

}
