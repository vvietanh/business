/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import Access.VonAccess;
import Model.VonModel;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author VIETANH
 */
@WebServlet(name = "VonServlet", urlPatterns = {"/VonServlet"})
public class VonServlet extends HttpServlet {

     VonAccess vonAccess = new VonAccess();
     VonModel vonModel = new VonModel();
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet VonServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet VonServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8"); 
            
            String command = request.getParameter("command");
            String mucvon = request.getParameter("txtMucVon");
            String donvi = request.getParameter("txtDonVi");
            String mavon = request.getParameter("MaVon");
            
            String url = "";
            try{
                switch(command){
                    
                    case "delete":
                        vonAccess.delete(Integer.parseInt(mavon));
                        url = "/business/View/Admin/Quanlyvon/danhsach.jsp";
                        break;
                }   
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url); 
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            
            String command = request.getParameter("command");
            String mucvon = request.getParameter("txtMucVon");
            String donvi = request.getParameter("txtDonVi");
            String mavon = request.getParameter("MaVon");
            
            String url = "", error = "";
            try{
                if(error.length() == 0){
                    switch(command){
                        case "insert":
                            vonAccess.insert(new VonModel(mucvon, donvi));
                            url = "/business/View/Admin/Quanlyvon/danhsach.jsp";
                            break;
                            
                        case "update":
                        vonAccess.update(Integer.parseInt(mavon), new VonModel(mucvon, donvi));
                        url = "/business/View/Admin/Quanlyvon/danhsach.jsp";
                        break;
                    }
                }
                else{
                    url = "/business/View/Admin/Quanlyvon/them.jsp";
                }
            }catch(Exception e){
            }
            response.setContentType("text/html; charset=UTF-8");
            response.setStatus(response.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", url);    
    }

   

}
