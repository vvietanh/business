/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Access.UserAccess;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Vi_Ngo
 */
@WebServlet(name = "KiemTraEmailServlet", urlPatterns = {"/KiemTraEmailServlet"})
public class KiemTraEmailServlet extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet KiemTraEmailServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet KiemTraEmailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    UserAccess userAccess = new UserAccess();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        if (userAccess.checkEmail(request.getParameter("email"))){
            response.getWriter().write("<img src=\"${root}/business/View/images/thumb-up-with-star(2).png\"/>");
        }else{
            response.getWriter().write("<img src=\"${root}/business/View/images/close.png\"/>");
        }
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
