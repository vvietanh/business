/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author VIETANH
 */
public class HinhDoanhNghiepModel {
    int MaHinh;
    String DuongDan;
    int MaDN;

    public HinhDoanhNghiepModel() {
    }

    public HinhDoanhNghiepModel(String DuongDan, int MaDN) {
        this.DuongDan = DuongDan;
        this.MaDN = MaDN;
    }

    public int getMaHinh() {
        return MaHinh;
    }

    public String getDuongDan() {
        return DuongDan;
    }

    public int getMaDN() {
        return MaDN;
    }

    public void setMaHinh(int MaHinh) {
        this.MaHinh = MaHinh;
    }

    public void setDuongDan(String DuongDan) {
        this.DuongDan = DuongDan;
    }

    public void setMaDN(int MaDN) {
        this.MaDN = MaDN;
    }

   
}
