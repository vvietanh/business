/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author Vi_Ngo
 */
public class BaiDangModel {
    private int maBD;
    private String tieuDe;
    private String noiDung;
    private String moTa;
    private Date ngayDang;
    private int trangThaiXem;
    private int trangThaiDuyet;
    private int nguoiDang;
    private int nguoiDuyet;

    public BaiDangModel() {
    }

    public BaiDangModel(int maBD, String tieuDe, String noiDung, String moTa, Date ngayDang, int trangThaiXem, int trangThaiDuyet, int nguoiDang, int nguoiDuyet) {
        this.maBD = maBD;
        this.tieuDe = tieuDe;
        this.noiDung = noiDung;
        this.moTa = moTa;
        this.ngayDang = ngayDang;
        this.trangThaiXem = trangThaiXem;
        this.trangThaiDuyet = trangThaiDuyet;
        this.nguoiDang = nguoiDang;
        this.nguoiDuyet = nguoiDuyet;
    }

    public int getMaBD() {
        return maBD;
    }

    public void setMaBD(int maBD) {
        this.maBD = maBD;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getNgayDang() {
        return ngayDang;
    }

    public void setNgayDang(Date ngayDang) {
        this.ngayDang = ngayDang;
    }

    public int getTrangThaiXem() {
        return trangThaiXem;
    }

    public void setTrangThaiXem(int trangThaiXem) {
        this.trangThaiXem = trangThaiXem;
    }

    public int getTrangThaiDuyet() {
        return trangThaiDuyet;
    }

    public void setTrangThaiDuyet(int trangThaiDuyet) {
        this.trangThaiDuyet = trangThaiDuyet;
    }

    public int getNguoiDang() {
        return nguoiDang;
    }

    public void setNguoiDang(int nguoiDang) {
        this.nguoiDang = nguoiDang;
    }

    public int getNguoiDuyet() {
        return nguoiDuyet;
    }

    public void setNguoiDuyet(int nguoiDuyet) {
        this.nguoiDuyet = nguoiDuyet;
    }
    
}
