/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Vi_Ngo
 */
public class HinhBaiDangModel {
    private int maHinh;
    private String duongDan;
    private int maBD;

    public HinhBaiDangModel() {
    }

    public HinhBaiDangModel(String duongDan, int maBD) {
        this.duongDan = duongDan;
        this.maBD = maBD;
    }

    public int getMaHinh() {
        return maHinh;
    }

    public void setMaHinh(int maHinh) {
        this.maHinh = maHinh;
    }

    public String getDuongDan() {
        return duongDan;
    }

    public void setDuongDan(String duongDan) {
        this.duongDan = duongDan;
    }

    public int getMaBD() {
        return maBD;
    }

    public void setMaBD(int maBD) {
        this.maBD = maBD;
    }
    
}
