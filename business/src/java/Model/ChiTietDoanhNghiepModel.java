/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author VIETANH
 */
public class ChiTietDoanhNghiepModel {
    int MaLV;
    int MaDN;

    public ChiTietDoanhNghiepModel() {
    }

    public ChiTietDoanhNghiepModel(int MaLV, int MaDN) {
        this.MaLV = MaLV;
        this.MaDN = MaDN;
    }

    public int getMaLV() {
        return MaLV;
    }

    public int getMaDN() {
        return MaDN;
    }

    public void setMaLV(int MaLV) {
        this.MaLV = MaLV;
    }

    public void setMaDN(int MaDN) {
        this.MaDN = MaDN;
    }
    
    
}
