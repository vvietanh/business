
package Model;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author ASUS
 */
public class TinTucModel {
    private Integer MaTin;
    private String TieuDe;
    private String NoiDung;
    private String MoTa;
    private String HinhAnh;
    private Date NgayDang;
    private int NguoiDang;

    public TinTucModel( String TieuDe, String NoiDung, String MoTa, String HinhAnh, Date NgayDang, int NguoiDang) {
        this.TieuDe = TieuDe;
        this.NoiDung = NoiDung;
        this.MoTa = MoTa;
        this.HinhAnh = HinhAnh;
        this.NgayDang = NgayDang;
        this.NguoiDang = NguoiDang;
    }

    public TinTucModel() {
      
    }

    public TinTucModel(String tieude) {
        
    }

   
  
        public Integer getMaTin() {
        return MaTin;
    }

    public void setMaTin(Integer MaTin) {
        this.MaTin = MaTin;
    }

    public String getTieuDe() {
        return TieuDe;
    }

    public void setTieuDe(String TieuDe) {
        this.TieuDe = TieuDe;
    }

    public String getNoiDung() {
        return NoiDung;
    }

    public void setNoiDung(String NoiDung) {
        this.NoiDung = NoiDung;
    }

    public String getMoTa() {
        return MoTa;
    }

    public void setMoTa(String MoTa) {
        this.MoTa = MoTa;
    }

    public String getHinhAnh() {
        return HinhAnh;
    }

    public void setHinhAnh(String HinhAnh) {
        this.HinhAnh = HinhAnh;
    }

    public Date getNgayDang() {
        return NgayDang;
    }

   public void setNgayDang(Date NgayDang) {
        this.NgayDang = NgayDang;
   }

    public int getNguoiDang() {
        return NguoiDang;
    }

    public void setNguoiDang(int NguoiDang) {
        this.NguoiDang = NguoiDang;
    }
    
   
}
