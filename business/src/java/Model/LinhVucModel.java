/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author VIETANH
 */
public class LinhVucModel {
    private Integer MaLV;
    private String TenLV;
    
    public LinhVucModel()
    {
    }

    public LinhVucModel(String TenLV) {
        this.TenLV = TenLV;
    }

    public Integer getMaLV() {
        return MaLV;
    }

    public String getTenLV() {
        return TenLV;
    }

    public void setMaLV(Integer MaLV) {
        this.MaLV = MaLV;
    }

    public void setTenLV(String TenLV) {
        this.TenLV = TenLV;
    }


    
    
}
