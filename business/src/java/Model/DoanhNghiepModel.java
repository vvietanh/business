/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Vi_Ngo
 */
public class DoanhNghiepModel {
    private int maDN;
    private String tenDN;
    private int maThue;
    private String diaChi;
    private int dienThoai;
    private int maVon;
    private int maBD;
    private int maKV;

    public DoanhNghiepModel() {
    }

    public DoanhNghiepModel(int maDN, String tenDN, int maThue, String diaChi, int dienThoai, int maVon, int maBD, int maKV) {
        this.maDN = maDN;
        this.tenDN = tenDN;
        this.maThue = maThue;
        this.diaChi = diaChi;
        this.dienThoai = dienThoai;
        this.maVon = maVon;
        this.maBD = maBD;
        this.maKV = maKV;
    }

    public DoanhNghiepModel(int i, String kim_Lien) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getMaDN() {
        return maDN;
    }

    public void setMaDN(int maDN) {
        this.maDN = maDN;
    }

    public String getTenDN() {
        return tenDN;
    }

    public void setTenDN(String tenDN) {
        this.tenDN = tenDN;
    }

    public int getMaThue() {
        return maThue;
    }

    public void setMaThue(int maThue) {
        this.maThue = maThue;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public int getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(int dienThoai) {
        this.dienThoai = dienThoai;
    }

    public int getVon() {
        return maVon;
    }

    public void setVon(int von) {
        this.maVon = von;
    }

    public int getMaBD() {
        return maBD;
    }

    public void setMaBD(int maBD) {
        this.maBD = maBD;
    }

    public int getMaKV() {
        return maKV;
    }

    public void setMaKV(int maKV) {
        this.maKV = maKV;
    }
    
}
