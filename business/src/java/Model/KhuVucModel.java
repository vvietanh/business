/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author VIETANH
 */
public class KhuVucModel {
    private Integer MaKV;
    private String TenKV;
    
    public KhuVucModel()
    {
    }

    public KhuVucModel(String TenKV) {
        this.TenKV = TenKV;
    }

    public Integer getMaKV() {
        return MaKV;
    }

    public String getTenKV() {
        return TenKV;
    }

    public void setMaKV(Integer MaKV) {
        this.MaKV = MaKV;
    }

    public void setTenKV(String TenKV) {
        this.TenKV = TenKV;
    }


    
    
}
