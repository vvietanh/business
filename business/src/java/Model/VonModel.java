/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author VIETANH
 */
public class VonModel {
    private Integer MaVon;
    private String MucVon;
    private String DonVi;
    
    public VonModel()
    {
    }

    public VonModel(String MucVon, String DonVi) {
        this.MucVon = MucVon;
        this.DonVi = DonVi;
    }

    public Integer getMaVon() {
        return MaVon;
    }

    public String getMucVon() {
        return MucVon;
    }

    public String getDonVi() {
        return DonVi;
    }

    public void setMaVon(Integer MaVon) {
        this.MaVon = MaVon;
    }

    public void setMucVon(String MucVon) {
        this.MucVon = MucVon;
    }

    public void setDonVi(String DonVi) {
        this.DonVi = DonVi;
    }
    

    


    
    
}
