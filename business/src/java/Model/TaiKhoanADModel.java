/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;


public class TaiKhoanADModel {
    private int maTK;
    private String hoTen;
    private String matKhau;
    private String email;
    private int dienThoai;
    private String diaChi;
    private int trangThai;
    private String anhDaiDien;
    public TaiKhoanADModel() {
    }

    public TaiKhoanADModel(String hoTen, String matKhau, String email, int dienThoai, String diaChi,int trangThai,String anhDaiDien) {
        this.hoTen = hoTen;
        this.matKhau = matKhau;
        this.email = email;
        this.dienThoai = dienThoai;
        this.diaChi = diaChi;
        this.trangThai = trangThai;
        this.anhDaiDien = anhDaiDien;
    }

    public TaiKhoanADModel(String hoten) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getAnhDaiDien() {
        return anhDaiDien;
    }

    public void setAnhDaiDien(String anhDaiDien) {
        this.anhDaiDien = anhDaiDien;
    }
    

    public int getMaTK() {
        return maTK;
    }

    public void setMaTK(int maTK) {
        this.maTK = maTK;
    }

    public String gethoTen() {
        return hoTen;
    }

    public void sethoTen(String tenDangNhap) {
        this.hoTen = hoTen;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(int dienThoai) {
        this.dienThoai = dienThoai;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }
    
}
